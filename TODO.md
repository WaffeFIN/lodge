Todo
======

###### To be done (top --> down)

*	Try coding expreiment/get feedback on syntax?
*	Figure out scoping!
	*	Should we go with functional style, where everything (except for the standard keywords) must otherwise be given to a function?
		*	Totally ass when importing functions... unless imported functions are global (in file) like `print` and `system`.
	*	JavaScript style, with closures? Might be easy to implement (just copy paste lol)
	*	Use the current wishywashy scoping for now, but add flattening operator?
*	Figure out how quoteless strings should be treated in different situations
*	Tuple shorthand with -s? As in Integers <=> Integer Tuple?
*	Data mutability. Have links? (Probably not)
	*	If a data value has only one reference, does each modification have to create new instances?
*	How to generics?
*	Update grammar: make more specific nodes to avoid guesswork later on.
*	(Semantic) Analyzing
	1.	Variable declaration checking (w/o imports)
	1.	Type checking
	1.	Other checks?
*	Running a given file
	1.	Comparators
	1.	Boolean 'and' and 'or'
	1.	"Map insertion" or prototyping
	1.	Better loop control
		*	end loop
		*	value returning loop
		*	for each
	1.	Think about conditional precedence (i.e. fix the ,?'s)
	1.	"Deep" assignment
*	Packaging/imports
	1.	Start on package manager
		1.	Import local .lodge files
		1.	Figure out .package file format
	1.	Dependency resolving (later)
*	Documentation
	1.	How to display/store?
*	Preprocessor project
	1.	Extract documentation
	1.	Remove `of` tokens
	1.	Remove `...` followed by new line

###### Already done

*	An RPG game (lol)
*	`infinite loop`
*	Functions and conditionals can execute statements within themselves
*	Values for each data type can be initialized (except for annotations/operators)
*	Added 10 simple code tests
*	Updated Lexer, Parser, Listeners and Visitors to version 2
*	Gedit syntax highlighting for Lodge
*	Second version of grammar
*	Started on interpreter
*	First version of grammar
