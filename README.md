Lodge
======

A cozy language.

```
"Hello world!" print	#prints Hello world!
```

The language paradigms (at least for now) are not completely clear. Constructs in the langauge are based on:

*	Class-based OOPLs (Mainly Smalltalk but also some Swift and possibly Java)
*	Prototype-oriented PLs (Self and JavaScript)
*	Functional PLs (Haskell and Clojure)

######Goals

1.	(At least somewhat) intuitive to code.
1.	Small and simple grammar with few semantic exceptions or special cases.
1.	Only a small amount of keywords.
1.	No way to alter the order of execution of a statement; statements are always executed from left to right.
1.	Data is described more often than acted upon (i.e. 4 doubled, instead of double(4))
1.	No restrictions on horisontal code identation.
1.	The language should be designed in a way that encourages clean code. See below.
1.	Design choices are made to emphasize expandability, understandability and simplicity of code.
	*	In that order.
	*	Code reusability should only be a byproduct following these goals.
1.	Logic is split into program and library code.
	*	Library code should be efficiently written, but easy to use.
	*	Program code uses the library code, and should be readable and understandable by beginners.
	*	Whether this is enforced or encouraged is still undetermined.
	
######Other features
	
*	Structurarly typed language with dynamic (duck) typing. Some static type checks.
*	Invariant based typing (a type defines some unbroken promises the value must hold).
*	All functions are limited to one input and output.

######Langauge overview: Data types and values

Lodge is based on JSON, which is noticable from the data types and syntax. There are twelve (12) kinds of data types:

*	Strings
	*	`"Hello!"`
	*	`"\"Don't quote me on this\"\n\t- me 2018"`
	*	Can also be defined without quotes
		*	It may contain any characters except for
			*	most punctuation characters `( ) [ ] { } \ | ' " , :`
			*	comment symbols `#`
			*	whitespace
		*	`Person`
		*	`@data`
		*	Quoteless strings differ slightly from normal Strings. More on that later.
*	Characters
	*	`'e'`
	*	`'\n'`
	*	`'\u09AF'`
*	Numbers
	*	`1`
	*	`-9.25`
	*	`314e-2`
*	Booleans
	*	`yes`
	*	`no`
*	Tuples
	*	`[ 0 | 1 | 1 | 1 | 2 | 3 | 5 | 9 ]`
*	Maps
	*	`{ some: "thing" | [0|1]: 100 | yes: undefined }`
*	Sets
	*	`{ "White" | "Red" | "Black" }`
	*	`{ [no|no] | [no|yes] | [yes|yes] }`
*	Functions
	*	`\|"Hello" print|`
	*	`\x| x + 1 |`
	*	`absDiff :: \pair to diff| diff: 0 pair - 1 pair . abs|`
*	Conditionals
	*	`? "this happens when given a truthy value" print | "false" print |`
	*	`bob . is Person ? "Hello" print||`
*	Errors
	*	`! "failed to load data" | data |`
	*	`input : input . is Empty String ? !"input must not be empty"|| | input |`
*	Operators
	*	`(Prefix Operator) successor (Positive Number to Positive Number) :: \n| n + 1|`
	*	`(Suffix Operator) times three :: \n| n * 3 |`
	*	`(Binary Operator) @ (Number Pair Pair to Positive Number) :: \vs to dist| dist :: [0 0 vs|0 1 vs] absDiff + [1 0 vs|1 1 vs] absDiff|`
*	and Undefined
	*	`undefined`
	
######The long list of standard values

Some values are initialized before running the program logic. To these include:

*	IO
	*	`system`
	*	`console`
	*	`print`
*	Packaging
	*	`package`
*	Control flow
	*	`loop`
*	Constants
	*	Boolean constants `yes` and `no`
	*	The `undefined` constant
*	Operators
	*	`, ^`  comma (chain) and power (repetition) operators
	*	`+ -`  addition and subtraction operators
	*	`= > < >= =<`  equality operators
	*	`to`  function type operator
*	Object types
	*	See chapter **Standard object types**

######Data mutability

TODO

######Transforming data

TODO

######Operator properties

When two values are conjoined without a specified operator, multiplication is used by default. For instance, `5 4 = 20`.

Note that function evaluation is done by the chain operator. For instance, `5, squared = 5 squared = 25`.

Exponentiation must satisfy the following equality

*	`a ^ n = a, a, ..., a` with n amount of a's on the right side of the equation for any `a` and positive number `n`
*	Iff an inverse is defined for `a`, then the equation above is defined for any integer `n`

Addition and subtraction

*	The addition and subtraction operators should satisfy the following equalities, for any `a` and `b`
*	`a + b = b + a`
*	TODO

Equality

*	`= > < >= =<`

######Standard object types

Types and subtypes...

*	Type
*	String
	*	Regex
	*	Base64
*	Number
	*	Byte
	*	Integer
	*	Decimal
	*	Positive
	*	Negative
*	Boolean
*	Tuple
	*	Singleton
	*	Pair
	*	Triplet
*	Error
	*	Type

And more...

######Typing

Types are essentially sets; you query the set whether or not a value is of a certain type.

```
Name (Type) :: {first: is String|last: is String} as Type
Person (Type) :: {name: is Name|age: is Positive Integer} as Type
Old (Type) :: {age: \a1|a1, is Positive Integer ? a1 \a2|a2 >= 60| | no |} as Type

bob page: {
	first name: "Bob"
	last name: "Page"
	age: 62
	billionaire: yes
}
bob page, is Person, print  #prints yes
bob page, is Old, print     #prints yes

{age: 212} is Person, print   #prints no
{age: 212} is Old, print      #prints yes
```

Extending other types

```
Employee :: {salary: Positive Integer} + Person
```

Enumerators work accordingly...

```
Hue (Type) :: {"Red"|"Orange"|"Yellow"|"Green"|"Blue"|"Indigo"|"Violet"} + Enum
Color (Type) :: {"Black"|"Gray"|"White"} + Hue

Gray is Hue, print    #prints no
Gray is Color, print  #prints yes
theme color (Color): "Blue"
theme color, print    #prints Blue
```

######Generics

Currently, there are none.

######Polymorphism and dispatch

TODO

######Examples: Simple functions

Hypotenuse

```
composed hypotenuse (Positive Number Tuple to Positive Number): squared, summed, square root
[5|12] composed hypotenuse, print #prints 13.0
```

Factorial

```
recursive factorial (Positive Integer to Positive Integer) :: \n to return|
	n = 0 ? return :: 1
	      | return :: n - 1, recursive factorial * n
		  |
|

7 recursive factorial, print #prints 5040

iterative factorial (Positive Integer to Positive Integer) :: \n to return|
	helper: \pair to pair|pair : [first of pair - 1 | first of pair * second of pair]| ^ n
	pair: [n | 1] helper  
	return :: second of pair
|

7 iterative factorial, print #prints 5040

PI :: Positive Integer
minimized iterative factorial (PI to PI) :: \n| f: \p|[0 p - 1|0 p * 1 p]|^ n
												p: [n|1] f
												0 p|
												
7 minimized iterative factorial, print #prints 5040
```
######Interpretation pipeline

1.	Given a lodge file, it is first preprocessed.
1.	Next, the `project.entry.lodge` file is tokenized, parsed, analyzed and run.
	1.	Here, the necessary constants must be initialized, and
	1.	The package manager must also be initialized
		*	Dependencies are downloaded, if missing
	1.	Lastly, the specified entry file (i.e. `main`) is run.
1.	The main interpretation loop is started.
	1.	Tokenize, parse, analyze and run the current file.
	1.	Each time a new package is used, it is interpreted and the interpreted code is cached temporarely.
1.	Interpretation stops when
	*	process is aborted manually,
	*	there is no commands left to be run, and
	*	at the instant when `exit program` is called.

######Preprocessing

*	`of`  tokens are removed. Exists only as syntactic sugar.
*	~~`-"-`  The rest of the line is identical to the one above, determined by token count~~ ''should be an IDE feature''
*	`...`  Statement continues on next line
*	`#`  Commented row
*	`##`  Start of a documentation block. All consecutive fully commented rows are treated as documentation for the next declaration.

######Preprocessing example

```
pair :: [':'|'D']
first of pair, print
second of pair, print

## Calculates something
# more details
#
# input: yep
# output: yup
long function name (Even Longer Type Names to Stuff) :: ...
	\type to whatever|
		#function stuff
	|
```

######Error handling

TODO

######Packaging

Importing

```
Functions from Math Utils package, imported here  #importing code
@data: Functions from Data Utils package  #using code, avoiding name clashing

3 ^ -1 * pi, tan, print
[0 | 1] mapify @data 
```

######Larger examples

TBD. A second repo will contain larger example programs. The following programs are planned to be included:

1.	Simple server program, with the possibility to login. Uses a lightweight HTTP framework and a simple database library.
2.	A JSON parser project.
3.	Something with an UI...

