grammar Lodge;

lodge: statements;

statements: (NEWLINE)* statement ((NEWLINE)+ statement)* (NEWLINE)*;

statement: assignment | expression;

assignment: (identifier_annotation)? constant_expression (type_annotation)? ASSIGNMENT_OPERATOR expression;
identifier_annotation: annotation;
type_annotation: annotation;

constant_expression: (value)+;

expression: (value | function | error | conditional)+;

value
	: '{' (NEWLINE)? assignments (separator)? '}'					#map
	| '{' (NEWLINE)? expressions (separator)? '}'					#set
	| '{' '}'														#empty_map
	| '[' (NEWLINE)? expressions (separator)? ']'					#tuple
	| '[' ']'														#empty_tuple
	| NUMBER														#number
	| BOOLEAN														#boolean
	| UNDEFINED														#undefined
	| CHARACTER														#character
	| STRING														#string
	| COMMA															#comma
	| QUOTELESS_STRING												#quoteless_string
	;	
	
annotation: '(' (NEWLINE)? expressions (separator)? ')';

function: (function_annotation)? FUNCTION_START (constant_expression)? BLOCK_END statements BLOCK_END;
function_annotation: annotation;

error: ERROR_START expression BLOCK_END (expression)? BLOCK_END;

conditional: CONDITIONAL_START statements BLOCK_END (statements)? BLOCK_END;

assignments: assignment (separator assignment)*;
expressions: expression (separator expression)*;

separator: BLOCK_END | NEWLINE;

FUNCTION_START: '\\';
ERROR_START: '!';
CONDITIONAL_START: '?';
BLOCK_END: '|';
ASSIGNMENT_OPERATOR: '::' | ':';

NEWLINE: '\r\n' | '\n';

BOOLEAN: 'yes' | 'no';
UNDEFINED: 'undefined';
COMMA: ',';

NUMBER: [+\-]? UNSIGNED_NUMBER;
UNSIGNED_NUMBER: REAL;
REAL: INTEGER ('.' [0-9]+)? EXPONENT?;
fragment INTEGER: '0' | [1-9] [0-9]*;
fragment EXPONENT: [Ee] [+\-]? INTEGER;

CHARACTER: '\'' (ESCAPE_CHARACTER | '\\\'' | ~['\\\n\r\f]) '\'';
STRING: '"' (ESCAPE_CHARACTER | '\\"' | ~["\\\n\r\f])* '"';
fragment ESCAPE_CHARACTER: '\\' ([\\/bfnrt] | UNICODE);
fragment UNICODE: 'u' (HEX)+;
fragment HEX: [0-9a-fA-F];

QUOTELESS_STRING: (~[()[\]{}\\|'",:?!# \t\n\r\f])+;

fragment WHITESPACE: [ \t]+;
fragment COMMENT: '#' ~[\n\r\f]*;

TRASH
	: (WHITESPACE | COMMENT) -> skip
	;
