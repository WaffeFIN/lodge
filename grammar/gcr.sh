antlr_path=/home/waxwax/Antlr/antlr
in=$1
cap="${in^}"
java -jar $antlr_path -visitor $cap.g4
javac -cp .:$antlr_path *.java 
java -cp .:$antlr_path org.antlr.v4.gui.TestRig $cap $in input.parser.$in -gui
