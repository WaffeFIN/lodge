package main;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Walter Grönholm
 */
public class LodgeTest {

    public LodgeTest() {
    }

    private String run(CharStream codeStream, String input) throws Exception {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try (PrintStream output = new PrintStream(byteArrayOutputStream, true, "utf-8")) {
            Lodge.run(codeStream, new Scanner(input), output);
            String outputString = new String(byteArrayOutputStream.toByteArray(), StandardCharsets.UTF_8);
            return outputString;
        }
    }

    @Test
    public void test_1_helloWorld() throws Exception {
        String code = "\"Hello World!\" print";
        String expectedOutput = "Hello World!";
        String input = "";

        CharStream codeStream = CharStreams.fromString(code);
        assertEquals(expectedOutput, run(codeStream, input));
    }

    @Test
    public void test_2_tuplesAndSets() throws Exception {
        String code = ""
            + "factorials: [1|1 2|1 2 3|1 2 3 4|1 2 3 4 5|1 2 3 4 5 6|1 2 3 4 5 6 7]\n"
            + "\"Here are the first 7 factorials: \" print\n"
            + "factorials, print\n"
            + "'\\n' print\n"
            + "set: {\"hello\"|1|yes}\n"
            + "yes in set, print\n"
            + "'\\n' print\n"
            + "1 in set, print\n"
            + "'\\n' print\n"
            + "\"hello\" in set, print\n"
            + "'\\n' print\n"
            + "no in set, print\n"
            + "'\\n' print\n"
            + "2 in set, print\n"
            + "'\\n' print\n"
            + "\"Hello\" in set, print\n"
            + "'\\n' print";
        String expectedOutput = ""
            + "Here are the first 7 factorials: [1, 2, 6, 24, 120, 720, 5040]\n"
            + "yes\n"
            + "yes\n"
            + "yes\n"
            + "no\n"
            + "no\n"
            + "no\n";
        String input = "";

        CharStream codeStream = CharStreams.fromString(code);
        assertEquals(expectedOutput, run(codeStream, input));
    }

    @Test
    public void test_3_conditional_expression() throws Exception {
        String code = ""
            + "conditional: ? \"hello\" | \"world\" |\n"
            + "yes conditional, print\n"
            + "'\\n' print\n"
            + "no conditional, print";
        String expectedOutput = ""
            + "hello\n"
            + "world";
        String input = "";

        CharStream codeStream = CharStreams.fromString(code);
        assertEquals(expectedOutput, run(codeStream, input));
    }

    @Test
    public void test_4_conditional_statements() throws Exception {
        String code = ""
            + "conditional: ? \"hello\" print\n"
            + "               '\\n' print\n"
            + "             | \"world\" print\n"
            + "             |\n"
            + "yes conditional\n"
            + "no conditional";
        String expectedOutput = ""
            + "hello\n"
            + "world";
        String input = "";

        CharStream codeStream = CharStreams.fromString(code);
        assertEquals(expectedOutput, run(codeStream, input));
    }

    @Test
    public void test_5_map_initialization() throws Exception {
        String code = ""
            + "map: {\n"
            + "\tage: 10\n"
            + "\tname: {first: \"Mark\"|last: \"Hamill\"}\n"
            + "}\n"
            + "\n"
            + "map, print";
        String expectedOutput = "{name={last=Hamill, first=Mark}, age=10}";
        String input = "";

        CharStream codeStream = CharStreams.fromString(code);
        assertEquals(expectedOutput, run(codeStream, input));
    }

    @Test
    public void test_6_simple_function_calls() throws Exception {
        String code = ""
            + "function: \\| \"hello\" print \n"
            + "             \" \"|\n"
            + "undefined function, print, function";
        String expectedOutput = "hello hello";
        String input = "";

        CharStream codeStream = CharStreams.fromString(code);
        assertEquals(expectedOutput, run(codeStream, input));
    }

    @Test
    public void test_7_function_with_identifier_in_super_scope() throws Exception {
        String code = ""
            + "state: 1\n"
            + "function: \\| state, print |\n"
            + "state: 2\n"
            + "undefined function";
        String expectedOutput = "2";
        String input = "";

        CharStream codeStream = CharStreams.fromString(code);
        assertEquals(expectedOutput, run(codeStream, input));
    }

    @Test
    public void test_8_function_input() throws Exception {
        String code = ""
            + "printed : \\x| x, print |\n"
            + "3 printed\n"
            + "'\\n' printed\n"
            + "squared : \\x| x, x |\n"
            + "3 squared, printed\n"
            + "'\\n' printed\n"
            + "4 squared, printed\n"
            + "'\\n' printed";
        String expectedOutput = "3\n9\n16\n";
        String input = "";

        CharStream codeStream = CharStreams.fromString(code);
        assertEquals(expectedOutput, run(codeStream, input));
    }

    @Test
    public void test_9_simple_infinite_loop() throws Exception {
        String code = ""
            + "values: [\"some\"|\"values\"|\"in\"|\"a\"|\"list\"|undefined]\n"
            + "\\index|\n"
            + "	index, values, print\n"
            + "	\" \" print\n"
            + "	index, values\n"
            + "| infinite loop";
        String expectedOutput = "some values in a list undefined ";
        String input = "";

        CharStream codeStream = CharStreams.fromString(code);
        assertEquals(expectedOutput, run(codeStream, input));
    }

    @Test
    public void test_10_ugly_hypotenuse() throws Exception {
        String code = ""
            + "root :: \\num|\n"
            + "	constNum :: num\n"
            + "	iterated: \\thing|\n"
            + "		constNum div thing + thing div 2\n"
            + "	|\n"
            + "	num: num, iterated\n"
            + "	num: num, iterated\n"
            + "	num: num, iterated\n"
            + "	num: num, iterated\n"
            + "	num: num, iterated\n"
            + "	num: num, iterated\n"
            + "	num: num, iterated\n"
            + "	num: num, iterated\n"
            + "	num: num, iterated\n"
            + "	num: num, iterated\n"
            + "	num: num, iterated\n"
            + "	num: num, iterated\n"
            + "	num\n"
            + "|\n"
            + "\n"
            + "hypotenuse :: \\pair|\n"
            + "	a2 : 0 pair, 0 pair\n"
            + "	b2 : 1 pair, 1 pair\n"
            + "	a2 + b2, root\n"
            + "|\n"
            + "\n"
            + "900 root, print\n"
            + "'\\n' print\n"
            + "[300|400] hypotenuse, print";
        String expectedOutput = ""
            + "30\n"
            + "500";
        String input = "";

        CharStream codeStream = CharStreams.fromString(code);
        assertEquals(expectedOutput, run(codeStream, input));
    }

    @Test
    public void test_11_echo() throws Exception {
        String code = ""
            + "\"Enter text\\n\" print\n"
            + "line: read line with console\n"
            + "\"You entered: \" + line, print";
        String expectedOutput = ""
            + "Enter text\n"
            + "You entered: This is a test";
        String input = "This is a test\n";

        CharStream codeStream = CharStreams.fromString(code);
        assertEquals(expectedOutput, run(codeStream, input));
    }

    @Test
    public void test_12_recursive_scope_check() throws Exception {
        String code = ""
            + "recursion: \\int|\n"
            + "	input: int\n"
            + "	int = 0, ? 1 recursion ||\n"
            + "	input + \" -> \" + int + '\\n', print\n"
            + "|\n"
            + "0 recursion";
        String expectedOutput = ""
            + "1 -> 1\n"
            + "0 -> 0\n";
        String input = "";

        CharStream codeStream = CharStreams.fromString(code);
        assertEquals(expectedOutput, run(codeStream, input));
    }
}
