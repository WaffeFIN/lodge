package common;

import common.values.ExceptionFactory;
import java.util.Objects;
import common.values.ILodgeValue;
import common.values.standard.LodgeType;
import common.values.standard.StandardLodgeTypes;

/**
 *
 * @author Walter Grönholm
 */
public class AnnotatedLodgeValue {

    public static enum OperatorType {
        NO_OPERATOR,
        PREFIX_OPERATOR,
        SUFFIX_OPERATOR,
        BINARY_OPERATOR
    }

    //flags
    private boolean mutable;
    private OperatorType operatorType;
    //underlying type
    private LodgeType type;
    private ILodgeValue value;

    public AnnotatedLodgeValue(LodgeType type, ILodgeValue value, boolean mutable, OperatorType operatorType) {
        this.type = type;
        this.value = value;
        this.mutable = mutable;
        this.operatorType = operatorType;
        validateValue();
    }

    public AnnotatedLodgeValue(LodgeType type, ILodgeValue value) {
        this(type, value, true, OperatorType.NO_OPERATOR);
    }

    private void validateValue() {
        if (!type.getValue().apply(value).getValue()) {
            throw ExceptionFactory.getTypeException(type.toString(), value);
        }
    }

    public void immutatify() {
        mutable = false;
    }

    public boolean isMutable() {
        return mutable;
    }

    public OperatorType getOperator() {
        return operatorType;
    }

    public ILodgeValue getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + (this.mutable ? 1 : 0);
        hash = 41 * hash + Objects.hashCode(this.type);
        hash = 41 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnnotatedLodgeValue other = (AnnotatedLodgeValue) obj;
        if (this.mutable != other.mutable) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("(");
        if (!mutable) {
            builder.append("C|");
        }
        if (operatorType != OperatorType.NO_OPERATOR) {
            builder.append(operatorType).append('|');
        }
        if (type != StandardLodgeTypes.Any) {
            builder.append(type).append('|');
        }
        String annotation = builder.append(')').toString();
        return "()".equals(annotation) ? value.toString() : annotation + value;
    }

}
