package common.values;

import common.exceptions.LodgeTypeException;
import common.exceptions.LodgeUndefinedValueException;

/**
 *
 * @author Walter Grönholm
 */
public class ExceptionFactory {

    public static LodgeTypeException getTypeException(String typeName, Object value) {
        return new LodgeTypeException("Expected a " + typeName + " but got: " + value);
    }

    public static LodgeUndefinedValueException getUndefinedValueException() {
        return new LodgeUndefinedValueException("Tried to get value of an undefined variable");
    }
}
