package common.values;

/**
 *
 * @author Walter Grönholm
 * @param <T>
 */
public interface ILodgeValue<T> {

    T getValue();

    ILodgeValue given(ILodgeValue value);
}
