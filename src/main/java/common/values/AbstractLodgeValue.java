package common.values;

import static common.values.standard.LodgeUndefined.undefined;
import java.util.Objects;

/**
 *
 * @author Walter Grönholm
 * @param <T> the underlying type
 */
public abstract class AbstractLodgeValue<T> implements ILodgeValue<T> {

    private T value;

    protected AbstractLodgeValue(T value) {
        this.value = value;
    }

    @Override
    public T getValue() {
        return value;
    }

    @Override
    public ILodgeValue given(ILodgeValue value) {
        return undefined;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractLodgeValue<?> other = (AbstractLodgeValue<?>) obj;
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return value.toString();
    }

}
