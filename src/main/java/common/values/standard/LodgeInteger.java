package common.values.standard;

import common.values.AbstractLodgeValue;
import common.values.ILodgeValue;

/**
 *
 * @author Walter Grönholm
 */
public class LodgeInteger extends AbstractLodgeValue<Integer> {

    LodgeInteger(Integer integer) {
        super(integer);
    }

    @Override
    public ILodgeValue given(ILodgeValue value) {
        if (value instanceof LodgeInteger) {
            Integer newValue = ((LodgeInteger) value).getValue() * getValue();
            return new LodgeInteger(newValue);
        }
        return super.given(value);
    }
    
    
}
