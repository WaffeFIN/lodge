package common.values.standard;

/**
 *
 * @author Walter Grönholm
 */
public class LodgeQuotelessString extends LodgeString {

    public LodgeQuotelessString(String string) {
        super(string);
    }

}
