package common.values.standard;

import common.values.ExceptionFactory;
import common.values.ILodgeValue;

/**
 *
 * @author Walter Grönholm
 */
public class LodgeUndefined implements ILodgeValue<Void> {

    public static LodgeUndefined undefined = new LodgeUndefined();

    LodgeUndefined() {
    }

    @Override
    public ILodgeValue given(ILodgeValue value) {
        return this;
    }

    @Override
    public Void getValue() {
        throw ExceptionFactory.getUndefinedValueException();
    }

    @Override
    public String toString() {
        return "undefined";
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof LodgeUndefined);
    }
    
    

}
