package common.values.standard;

import common.AnnotatedLodgeValue;
import common.values.AbstractLodgeValue;
import common.values.ILodgeValue;
import static common.values.standard.LodgeUndefined.undefined;
import java.util.Map;

/**
 *
 * @author Walter Grönholm
 */
public class LodgeMap extends AbstractLodgeValue<Map<ILodgeValue, AnnotatedLodgeValue>> {
    
    LodgeMap(Map<ILodgeValue, AnnotatedLodgeValue> value) {
        super(value);
    }

    @Override
    public ILodgeValue given(ILodgeValue value) {
        AnnotatedLodgeValue returnValue = getValue().get(value);
        return returnValue == null ? undefined : returnValue.getValue();
    }
    
    
    
}