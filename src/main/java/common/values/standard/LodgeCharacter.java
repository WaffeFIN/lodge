package common.values.standard;

import common.values.AbstractLodgeValue;

/**
 *
 * @author Walter Grönholm
 */
public class LodgeCharacter extends AbstractLodgeValue<Character> {

    LodgeCharacter(Character character) {
        super(character);
    }
}
