package common.values.standard;

import common.values.AbstractLodgeValue;
import common.values.ILodgeValue;
import java.util.function.BiFunction;

/**
 *
 * @author Walter Grönholm
 */
public class LodgeOperator extends AbstractLodgeValue<BiFunction<ILodgeValue, ILodgeValue, ILodgeValue>> {

    LodgeOperator(BiFunction<ILodgeValue, ILodgeValue, ILodgeValue> value) {
        super(value);
    }

    @Override
    public ILodgeValue given(ILodgeValue value) {
        return operate(value.given(new LodgeInteger(0)), value.given(new LodgeInteger(1)));
    }

    public ILodgeValue operate(ILodgeValue left, ILodgeValue right) {
        return getValue().apply(left, right);
    }

}
