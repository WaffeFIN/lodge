package common.values.standard;

import common.values.AbstractLodgeValue;
import common.values.ILodgeValue;
import java.util.List;

/**
 *
 * @author Walter Grönholm
 */
public class LodgeTuple extends AbstractLodgeValue<List<ILodgeValue>> {

    LodgeTuple(List<ILodgeValue> value) {
        super(value);
    }

    @Override
    public ILodgeValue given(ILodgeValue value) {
        ILodgeValue returnValue = null;
        if (value instanceof LodgeInteger) {
            Integer integerValue = ((LodgeInteger) value).getValue();
            returnValue = getReturnValue(integerValue);
        }
        return returnValue == null ? super.given(value) : returnValue;
    }

    private ILodgeValue getReturnValue(Integer integerValue) {
        if (integerValue < 0) {
            integerValue = getValue().size() + integerValue;
        }
        return getValue().get(integerValue);
    }

}
