package common.values.standard;

import common.values.AbstractLodgeValue;
import common.values.ILodgeValue;
import static common.values.standard.LodgeUndefined.undefined;

/**
 *
 * @author Walter Grönholm
 */
public class LodgeString extends AbstractLodgeValue<String> {

    public LodgeString(String string) {
        super(string);
    }

    @Override
    public ILodgeValue given(ILodgeValue value) {
        ILodgeValue returnValue = null;
        if (value instanceof LodgeString) {
            String stringValue = ((LodgeString) value).getValue();
            returnValue = getReturnValue(stringValue);
        } else if (value instanceof LodgeInteger) {
            Integer integerValue = ((LodgeInteger) value).getValue();
            returnValue = getReturnValue(integerValue);
        }
        return returnValue == null ? super.given(value) : returnValue;
    }

    private ILodgeValue getReturnValue(String stringValue) {
        switch (stringValue) {
            case "reverse":
                return new LodgeString(new StringBuilder(getValue()).reverse().toString());
            case "uppercase":
                return new LodgeString(getValue().toUpperCase());
            case "lowercase":
                return new LodgeString(getValue().toLowerCase());
        }
        return null;
    }

    private ILodgeValue getReturnValue(Integer integerValue) {
        if (integerValue < 0) {
            integerValue = getValue().length() + integerValue;
        }
        if (integerValue < getValue().length()) {
            return new LodgeCharacter(getValue().charAt(integerValue));
        } else {
            return undefined;
        }
    }
}
