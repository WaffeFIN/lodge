package common.values.standard;

import common.values.AbstractLodgeValue;
import common.values.ExceptionFactory;
import common.values.ILodgeValue;
import java.util.function.Function;

/**
 *
 * @author Walter Grönholm
 */
public class LodgeConditional extends AbstractLodgeValue<Function<LodgeBoolean, LodgeFunction>> {
    
    LodgeConditional(Function<LodgeBoolean, LodgeFunction> value) {
        super(value);
    }

    @Override
    public ILodgeValue given(ILodgeValue value) {
        if (value instanceof LodgeBoolean)
            return given((LodgeBoolean) value);
        if (value instanceof LodgeUndefined)
            return given(new LodgeBoolean(false));
        throw ExceptionFactory.getTypeException("Boolean", value);
    }

    private ILodgeValue given(LodgeBoolean value) {        
        LodgeFunction func = getValue().apply(value);
        return func.getValue().apply(new LodgeUndefined());
    }
    
}