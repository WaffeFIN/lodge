package common.values.standard;

import common.values.AbstractLodgeValue;
import common.values.ILodgeValue;
import java.util.function.Function;

/**
 *
 * @author Walter Grönholm
 */
public class LodgeType extends AbstractLodgeValue<Function<ILodgeValue, LodgeBoolean>> {
    
    LodgeType(Function<ILodgeValue, LodgeBoolean> value) {
        super(value);
    }
    
}
