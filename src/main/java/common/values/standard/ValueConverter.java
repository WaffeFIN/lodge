package common.values.standard;

import common.values.ExceptionFactory;
import common.values.ILodgeValue;
import static common.values.standard.LodgeUndefined.undefined;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.commons.text.StringEscapeUtils;
import parsing.LodgeParser;
import visiting.running.IStatementHandler;

/**
 *
 * @author Walter Grönholm
 */
public class ValueConverter implements IValueConverter {

    private final IStatementHandler handler;

    public ValueConverter(IStatementHandler evaluator) {
        this.handler = evaluator;
    }

    @Override
    public LodgeUndefined convert(LodgeParser.UndefinedContext context) {
        return undefined;
    }

    @Override
    public LodgeString convert(LodgeParser.StringContext context) {
        String quoted = StringEscapeUtils.unescapeJava(context.getText());
        return new LodgeString(quoted.substring(1, quoted.length() - 1));
    }

    @Override
    public LodgeQuotelessString convert(LodgeParser.Quoteless_stringContext context) {
        return new LodgeQuotelessString(context.getText());
    }

    @Override
    public LodgeQuotelessString convert(LodgeParser.CommaContext context) {
        return new LodgeQuotelessString(context.getText());
    }

    @Override
    public LodgeSet convert(LodgeParser.SetContext context) {
        Set<ILodgeValue> set = new HashSet<>();
        List<LodgeParser.ExpressionContext> expressions = context.expressions().expression();
        for (LodgeParser.ExpressionContext expression : expressions) {
            set.add(handler.calculateExpression(expression));
        }
        return new LodgeSet(set);
    }

    @Override
    public LodgeMap convert(LodgeParser.MapContext context) {
        return convertToMap(context.assignments().assignment());
    }

    @Override
    public LodgeMap convert(LodgeParser.Empty_mapContext context) {
        return convertToMap(new ArrayList<>());
    }

    private LodgeMap convertToMap(List<LodgeParser.AssignmentContext> assignments) {
        handler.pushScope();
        for (LodgeParser.AssignmentContext assignment : assignments) {
            handler.assign(assignment);
        }
        return new LodgeMap(handler.popScope());
    }

    @Override
    public LodgeInteger convert(LodgeParser.NumberContext context) {
        return new LodgeInteger(Integer.parseInt(context.getText()));
    }

    private int getRow(ParserRuleContext context) {
        return context.start.getLine();
    }

    private int getColumn(ParserRuleContext context) {
        return context.start.getCharPositionInLine();
    }

    @Override
    public LodgeError convert(LodgeParser.ErrorContext context) {
        StringBuilder messageBuilder = new StringBuilder();
        int row = getRow(context);
        int column = getColumn(context);
        messageBuilder.append(
            buildErrorMessage(
                handler.calculateExpression(context.expression(0)),
                row,
                column
            )
        );
        messageBuilder.append('\n');
        if (context.expression(1) != null) {
            messageBuilder.append(handler.calculateExpression(context.expression(1)));
        }

        return new LodgeError(messageBuilder.toString());
    }

    private String buildErrorMessage(Object message, int row, int column) {
        return "       Error @" + row + ':' + column + "\twith message " + message;
    }

    @Override
    public LodgeConditional convert(LodgeParser.ConditionalContext context) {
        Function<LodgeBoolean, LodgeFunction> function = (LodgeBoolean t1) -> {
            if (t1.getValue()) {
                return convertToFunction(null, null, context.statements(0));
            }
            if (context.statements(1) != null) {
                return convertToFunction(null, null, context.statements(1));
            }
            return new LodgeFunction((t2) -> undefined);
        };
        return new LodgeConditional(function);
    }

    @Override
    public LodgeFunction convert(LodgeParser.FunctionContext context) {
        LodgeString in = null;
        LodgeString out = null;
        if (context.constant_expression() != null) {
            LodgeParser.ValueContext inContext = context.constant_expression().value().get(0);
            if (inContext != null) {
                if (inContext instanceof LodgeParser.Quoteless_stringContext) {
                    in = convert((LodgeParser.Quoteless_stringContext) inContext);
                } else if (inContext instanceof LodgeParser.StringContext) {
                    in = convert((LodgeParser.StringContext) inContext);
                } else {
                    throw ExceptionFactory.getTypeException("String", inContext);
                }
            }
        }
        return convertToFunction(in, out, context.statements());
    }

    private LodgeFunction convertToFunction(LodgeString in, LodgeString out, LodgeParser.StatementsContext statements) {
        return new LodgeFunction((t) -> {
            handler.pushScope();
            if (in != null) {
                handler.assign(in, t, true);
            }
            ILodgeValue returnValue = undefined;
            for (int i = 0; i < statements.statement().size(); i++) {
                LodgeParser.StatementContext statement = statements.statement(i);
                returnValue = handleStatement(statement);
                if (i == statements.statement().size() - 1 && out != null) {
                    returnValue = handler.getVariable(out).getValue();
                }
            }
            handler.popScope();
            return returnValue;
        });
    }

    private ILodgeValue handleStatement(LodgeParser.StatementContext statement) {
        if (statement.assignment() != null) {
            return handler.assign(statement.assignment());
        } else {
            return handler.calculateExpression(statement.expression());
        }
    }

    @Override
    public LodgeCharacter convert(LodgeParser.CharacterContext context) {
        return new LodgeCharacter(StringEscapeUtils.unescapeJava(context.getText()).charAt(1));
    }

    @Override
    public LodgeBoolean convert(LodgeParser.BooleanContext context) {
        return new LodgeBoolean("yes".equals(context.getText()));
    }

    @Override
    public LodgeTuple convert(LodgeParser.TupleContext context) {
        return convertToTuple(context.expressions().expression());
    }

    @Override
    public LodgeTuple convert(LodgeParser.Empty_tupleContext context) {
        return convertToTuple(new ArrayList<>());
    }

    private LodgeTuple convertToTuple(List<LodgeParser.ExpressionContext> expressions) {
        List<ILodgeValue> values = new ArrayList<>(expressions.size());
        for (LodgeParser.ExpressionContext expression : expressions) {
            values.add(handler.calculateExpression(expression));
        }
        return new LodgeTuple(values);
    }
}
