package common.values.standard;

import common.values.AbstractLodgeValue;
import common.values.ILodgeValue;

/**
 *
 * @author Walter Grönholm
 */
public class LodgeBoolean extends AbstractLodgeValue<Boolean> {

    LodgeBoolean(Boolean bool) {
        super(bool);
    }

    @Override
    public ILodgeValue given(ILodgeValue value) {
        ILodgeValue returnValue = null;
        if (value instanceof LodgeString) {
            String stringValue = ((LodgeString) value).getValue();
            returnValue = getReturnValue(stringValue);
        }
        return returnValue == null ? super.given(value) : returnValue;
    }

    @Override
    public String toString() {
        return getValue() ? "yes" : "no";
    }

    private ILodgeValue getReturnValue(String stringValue) {
        switch (stringValue) {
            case "not":
                return new LodgeBoolean(!getValue());
            default:
                return null;
        }
    }

}
