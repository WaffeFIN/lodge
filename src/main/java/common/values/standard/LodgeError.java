package common.values.standard;

import common.values.AbstractLodgeValue;

/**
 *
 * @author Walter Grönholm
 */
public class LodgeError extends AbstractLodgeValue<String> {
    
    LodgeError(String message) {
        super(message);
    }
    
}
