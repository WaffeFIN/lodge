package common.values.standard;

import common.values.AbstractLodgeValue;
import common.values.ILodgeValue;
import java.util.function.Function;

/**
 *
 * @author Walter Grönholm
 */
public class LodgeFunction extends AbstractLodgeValue<Function<ILodgeValue, ILodgeValue>> {
    
    LodgeFunction(Function<ILodgeValue, ILodgeValue> value) {
        super(value);
    }

    @Override
    public ILodgeValue given(ILodgeValue value) {
        return getValue().apply(value);
    }
    
}
