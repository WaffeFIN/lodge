package common.values.standard;

import common.values.ILodgeValue;
import parsing.LodgeParser;

/**
 *
 * @author Walter Grönholm
 */
public interface IValueConverter {
    
    public LodgeUndefined convert(LodgeParser.UndefinedContext context);

    public LodgeQuotelessString convert(LodgeParser.Quoteless_stringContext context);
    
    public LodgeQuotelessString convert(LodgeParser.CommaContext context);

    public LodgeString convert(LodgeParser.StringContext context);

    public LodgeSet convert(LodgeParser.SetContext context);

    public LodgeMap convert(LodgeParser.MapContext context);

    public LodgeMap convert(LodgeParser.Empty_mapContext context);

    public LodgeInteger convert(LodgeParser.NumberContext context);

    public LodgeFunction convert(LodgeParser.FunctionContext context);

    public LodgeError convert(LodgeParser.ErrorContext context);

    public LodgeConditional convert(LodgeParser.ConditionalContext context);

    public LodgeCharacter convert(LodgeParser.CharacterContext context);

    public LodgeBoolean convert(LodgeParser.BooleanContext context);

    public LodgeTuple convert(LodgeParser.TupleContext context);

    public LodgeTuple convert(LodgeParser.Empty_tupleContext context);
}
