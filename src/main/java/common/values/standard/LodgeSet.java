package common.values.standard;

import common.values.AbstractLodgeValue;
import common.values.ILodgeValue;
import java.util.Set;

/**
 *
 * @author Walter Grönholm
 */
public class LodgeSet extends AbstractLodgeValue<Set<ILodgeValue>> {

    LodgeSet(Set<ILodgeValue> value) {
        super(value);
    }

    @Override
    public ILodgeValue given(ILodgeValue value) {
        ILodgeValue returnValue = null;
        if (value instanceof LodgeString) {
            String stringValue = ((LodgeString) value).getValue();
            returnValue = getReturnValue(stringValue);
        }
        return returnValue == null ? super.given(value) : returnValue;
    }

    private ILodgeValue getReturnValue(String stringValue) {
        switch (stringValue) {
            case "in":
                return new LodgeFunction((val) -> new LodgeBoolean(getValue().contains(val)));
        }
        return null;
    }

}
