package common.values.standard;

/**
 *
 * @author Walter Grönholm
 */
public class StandardLodgeTypes {
    
    public static final LodgeType Any = new LodgeType(val -> {
        return new LodgeBoolean(true);
    });

    public static final LodgeType Type = new LodgeType(val -> {
        return new LodgeBoolean(val instanceof LodgeType);
    });

    public static final LodgeType String = new LodgeType(val -> {
        return new LodgeBoolean(val instanceof LodgeString);
    });
    
    public static final LodgeType Integer = new LodgeType(val -> {
        return new LodgeBoolean(val instanceof LodgeInteger);
    });
    
    public static final LodgeType Boolean = new LodgeType(val -> {
        return new LodgeBoolean(val instanceof LodgeBoolean);
    });
}
