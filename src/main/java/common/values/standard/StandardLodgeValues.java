package common.values.standard;

import common.AnnotatedLodgeValue;
import common.values.ILodgeValue;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;

/**
 *
 * @author Walter Grönholm
 */
public class StandardLodgeValues {

    public final LodgeFunction print;
    public final LodgeFunction line_with_console;
    public final LodgeFunction with_console;
    public final LodgeFunction console;

    public StandardLodgeValues(Scanner in, PrintStream out) {
        print = new LodgeFunction((val) -> {
            out.print(val);
            return val;
        });
        line_with_console = new LodgeFunction((val) -> {
            return new LodgeString(in.nextLine());
        });
        with_console = getByConsole();
        console = getConsole();
    }

    public final LodgeUndefined undefined = new LodgeUndefined();

    public final LodgeFunction comma = new LodgeFunction((val) -> val);

    public final LodgeFunction infinite_loop = new LodgeFunction((val) -> {
        LodgeFunction func = (LodgeFunction) val;
        LodgeInteger index = new LodgeInteger(0);
        while (true) {
            ILodgeValue returnValue = func.given(index);
            if (undefined.equals(returnValue)) {
                break;
            }
            index = new LodgeInteger(index.getValue() + 1);
        }
        return undefined;
    });

    private LodgeFunction getN_loop(int n) {
        return new LodgeFunction((val) -> {
            LodgeFunction func = (LodgeFunction) val;
            for (LodgeInteger index = new LodgeInteger(0); index.getValue() < n; index = new LodgeInteger(index.getValue() + 1)) {
                ILodgeValue returnValue = func.given(index);
                if (undefined.equals(returnValue)) {
                    break;
                }
            }
            return undefined;
        });
    }

    public final LodgeFunction loop = new LodgeFunction(getLoopMap());

    private Function<ILodgeValue, ILodgeValue> getLoopMap() {
        Map<ILodgeValue, AnnotatedLodgeValue> map = new HashMap<>();
        map.put(new LodgeQuotelessString("infinite"), new AnnotatedLodgeValue(
            StandardLodgeTypes.Any,
            infinite_loop,
            false,
            AnnotatedLodgeValue.OperatorType.NO_OPERATOR));
        return (val) -> {
            if (val instanceof LodgeInteger) {
                return getN_loop(((LodgeInteger) val).getValue());
            }
            AnnotatedLodgeValue anntoatedValue = map.get(val);
            return anntoatedValue == null ? undefined : anntoatedValue.getValue();
        };
    }

    public final LodgeOperator plus = new LodgeOperator((left, right) -> {
        if ((left instanceof LodgeInteger) && (right instanceof LodgeInteger)) {
            return new LodgeInteger(((LodgeInteger) left).getValue() + ((LodgeInteger) right).getValue());
        }
        if ((left instanceof LodgeString) && (right instanceof LodgeString)) {
            return new LodgeString(((LodgeString) left).getValue() + ((LodgeString) right).getValue());
        }
        if ((left instanceof LodgeCharacter) && (right instanceof LodgeString)) {
            return new LodgeString(((LodgeCharacter) left).getValue() + ((LodgeString) right).getValue());
        }
        if ((left instanceof LodgeString) && (right instanceof LodgeCharacter)) {
            return new LodgeString(((LodgeString) left).getValue() + ((LodgeCharacter) right).getValue());
        }
        if ((left instanceof LodgeInteger) && (right instanceof LodgeString)) {
            return new LodgeString(((LodgeInteger) left).getValue() + ((LodgeString) right).getValue());
        }
        if ((left instanceof LodgeString) && (right instanceof LodgeInteger)) {
            return new LodgeString(((LodgeString) left).getValue() + ((LodgeInteger) right).getValue());
        }
        return undefined;
    });

    public final LodgeOperator minus = new LodgeOperator((left, right) -> {
        if ((left instanceof LodgeInteger) && (right instanceof LodgeInteger)) {
            return new LodgeInteger(((LodgeInteger) left).getValue() - ((LodgeInteger) right).getValue());
        }
        if ((left instanceof LodgeUndefined) && (right instanceof LodgeInteger)) {
            return new LodgeInteger(-((LodgeInteger) right).getValue());
        }
        if ((left instanceof LodgeInteger) && (right instanceof LodgeUndefined)) {
            return left;
        }
        if ((left instanceof LodgeString) && (right instanceof LodgeString)) {
            String leftString = ((LodgeString) left).getValue();
            String rightString = ((LodgeString) right).getValue();
            if (leftString.endsWith(rightString)) {
                return new LodgeString(leftString.substring(0, leftString.length() - rightString.length()));
            } else {
                return left;
            }
        }
        return undefined;
    });

    public final LodgeOperator equals = new LodgeOperator((left, right) -> {
        return new LodgeBoolean(right.equals(left));
    });

    public final ILodgeValue division = new LodgeOperator((left, right) -> {
        if ((left instanceof LodgeInteger) && (right instanceof LodgeInteger)) {
            return new LodgeInteger(((LodgeInteger) left).getValue() / ((LodgeInteger) right).getValue());
        }
        return undefined;
    });
    
    public final LodgeFunction program = new LodgeFunction((val) -> {
        if (val instanceof LodgeString) {
            switch (((LodgeString) val).getValue()) {
                case "exit":
                    System.exit(0);
            }
        }
        return undefined;
    });

    private LodgeFunction getByConsole() {
        return new LodgeFunction(getWithConsoleMap());
    }

    private LodgeFunction getConsole() {
        return new LodgeFunction(getConsoleMap());
    }

    private Function<ILodgeValue, ILodgeValue> getWithConsoleMap() {
        Map<ILodgeValue, AnnotatedLodgeValue> map = new HashMap<>();
        map.put(new LodgeQuotelessString("line"), new AnnotatedLodgeValue(
            StandardLodgeTypes.Any,
            line_with_console,
            false,
            AnnotatedLodgeValue.OperatorType.NO_OPERATOR));
        return (val) -> {
            AnnotatedLodgeValue anntoatedValue = map.get(val);
            return anntoatedValue == null ? undefined : anntoatedValue.getValue();
        };
    }

    private Function<ILodgeValue, ILodgeValue> getConsoleMap() {
        Map<ILodgeValue, AnnotatedLodgeValue> map = new HashMap<>();
        map.put(new LodgeQuotelessString("with"), new AnnotatedLodgeValue(
            StandardLodgeTypes.Any,
            with_console,
            false,
            AnnotatedLodgeValue.OperatorType.NO_OPERATOR));
        return (val) -> {
            AnnotatedLodgeValue anntoatedValue = map.get(val);
            return anntoatedValue == null ? undefined : anntoatedValue.getValue();
        };
    }
}
