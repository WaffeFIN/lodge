package common.exceptions;

import main.AbstractLodgeException;

/**
 *
 * @author Walter Grönholm
 */
public class LodgeRuntimeException extends AbstractLodgeException {

    public LodgeRuntimeException(String message) {
        super(message);
    }

}
