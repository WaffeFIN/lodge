package common.exceptions;

import main.AbstractLodgeException;

/**
 * Thrown when attempting to get a value from an undefined variable.
 *
 * @author Walter Grönholm
 */
public class LodgeUndefinedValueException extends AbstractLodgeException {

    public LodgeUndefinedValueException(String message) {
        super(message);
    }

}
