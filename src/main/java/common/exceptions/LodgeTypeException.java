package common.exceptions;

import main.AbstractLodgeException;

/**
 * Thrown when there is a type mismatch.
 *
 * @author Walter Grönholm
 */
public class LodgeTypeException extends AbstractLodgeException {

    public LodgeTypeException(String message) {
        super(message);
    }

}
