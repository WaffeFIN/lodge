package parsing;
// Generated from Lodge.g4 by ANTLR 4.7.1

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import listening.ILodgeListener;
import visiting.ILodgeVisitor;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class LodgeParser extends Parser {

    static {
        RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION);
    }

    protected static final DFA[] _decisionToDFA;
    protected static final PredictionContextCache _sharedContextCache
        = new PredictionContextCache();
    public static final int T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, FUNCTION_START = 7, ERROR_START = 8,
        CONDITIONAL_START = 9, BLOCK_END = 10, ASSIGNMENT_OPERATOR = 11, NEWLINE = 12,
        BOOLEAN = 13, UNDEFINED = 14, COMMA = 15, NUMBER = 16, UNSIGNED_NUMBER = 17, REAL = 18,
        CHARACTER = 19, STRING = 20, QUOTELESS_STRING = 21, TRASH = 22;
    public static final int RULE_lodge = 0, RULE_statements = 1, RULE_statement = 2, RULE_assignment = 3,
        RULE_identifier_annotation = 4, RULE_type_annotation = 5, RULE_constant_expression = 6,
        RULE_expression = 7, RULE_value = 8, RULE_annotation = 9, RULE_function = 10,
        RULE_function_annotation = 11, RULE_error = 12, RULE_conditional = 13,
        RULE_assignments = 14, RULE_expressions = 15, RULE_separator = 16;
    public static final String[] ruleNames = {
        "lodge", "statements", "statement", "assignment", "identifier_annotation",
        "type_annotation", "constant_expression", "expression", "value", "annotation",
        "function", "function_annotation", "error", "conditional", "assignments",
        "expressions", "separator"
    };

    private static final String[] _LITERAL_NAMES = {
        null, "'{'", "'}'", "'['", "']'", "'('", "')'", "'\\'", "'!'", "'?'",
        "'|'", null, null, null, "'undefined'", "','"
    };
    private static final String[] _SYMBOLIC_NAMES = {
        null, null, null, null, null, null, null, "FUNCTION_START", "ERROR_START",
        "CONDITIONAL_START", "BLOCK_END", "ASSIGNMENT_OPERATOR", "NEWLINE", "BOOLEAN",
        "UNDEFINED", "COMMA", "NUMBER", "UNSIGNED_NUMBER", "REAL", "CHARACTER",
        "STRING", "QUOTELESS_STRING", "TRASH"
    };
    public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

    /**
     * @deprecated Use {@link #VOCABULARY} instead.
     */
    @Deprecated
    public static final String[] tokenNames;

    static {
        tokenNames = new String[_SYMBOLIC_NAMES.length];
        for (int i = 0; i < tokenNames.length; i++) {
            tokenNames[i] = VOCABULARY.getLiteralName(i);
            if (tokenNames[i] == null) {
                tokenNames[i] = VOCABULARY.getSymbolicName(i);
            }

            if (tokenNames[i] == null) {
                tokenNames[i] = "<INVALID>";
            }
        }
    }

    @Override
    @Deprecated
    public String[] getTokenNames() {
        return tokenNames;
    }

    @Override

    public Vocabulary getVocabulary() {
        return VOCABULARY;
    }

    @Override
    public String getGrammarFileName() {
        return "Lodge.g4";
    }

    @Override
    public String[] getRuleNames() {
        return ruleNames;
    }

    @Override
    public String getSerializedATN() {
        return _serializedATN;
    }

    @Override
    public ATN getATN() {
        return _ATN;
    }

    public LodgeParser(TokenStream input) {
        super(input);
        _interp = new ParserATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
    }

    public static class LodgeContext extends ParserRuleContext {

        public StatementsContext statements() {
            return getRuleContext(StatementsContext.class, 0);
        }

        public LodgeContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_lodge;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterLodge(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitLodge(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitLodge(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final LodgeContext lodge() throws RecognitionException {
        LodgeContext _localctx = new LodgeContext(_ctx, getState());
        enterRule(_localctx, 0, RULE_lodge);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(34);
                statements();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class StatementsContext extends ParserRuleContext {

        public List<StatementContext> statement() {
            return getRuleContexts(StatementContext.class);
        }

        public StatementContext statement(int i) {
            return getRuleContext(StatementContext.class, i);
        }

        public List<TerminalNode> NEWLINE() {
            return getTokens(LodgeParser.NEWLINE);
        }

        public TerminalNode NEWLINE(int i) {
            return getToken(LodgeParser.NEWLINE, i);
        }

        public StatementsContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_statements;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterStatements(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitStatements(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitStatements(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final StatementsContext statements() throws RecognitionException {
        StatementsContext _localctx = new StatementsContext(_ctx, getState());
        enterRule(_localctx, 2, RULE_statements);
        int _la;
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(39);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == NEWLINE) {
                    {
                        {
                            setState(36);
                            match(NEWLINE);
                        }
                    }
                    setState(41);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                setState(42);
                statement();
                setState(51);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 2, _ctx);
                while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                    if (_alt == 1) {
                        {
                            {
                                setState(44);
                                _errHandler.sync(this);
                                _la = _input.LA(1);
                                do {
                                    {
                                        {
                                            setState(43);
                                            match(NEWLINE);
                                        }
                                    }
                                    setState(46);
                                    _errHandler.sync(this);
                                    _la = _input.LA(1);
                                } while (_la == NEWLINE);
                                setState(48);
                                statement();
                            }
                        }
                    }
                    setState(53);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 2, _ctx);
                }
                setState(57);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == NEWLINE) {
                    {
                        {
                            setState(54);
                            match(NEWLINE);
                        }
                    }
                    setState(59);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class StatementContext extends ParserRuleContext {

        public AssignmentContext assignment() {
            return getRuleContext(AssignmentContext.class, 0);
        }

        public ExpressionContext expression() {
            return getRuleContext(ExpressionContext.class, 0);
        }

        public StatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_statement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterStatement(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitStatement(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitStatement(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final StatementContext statement() throws RecognitionException {
        StatementContext _localctx = new StatementContext(_ctx, getState());
        enterRule(_localctx, 4, RULE_statement);
        try {
            setState(62);
            _errHandler.sync(this);
            switch (getInterpreter().adaptivePredict(_input, 4, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                     {
                        setState(60);
                        assignment();
                    }
                    break;
                case 2:
                    enterOuterAlt(_localctx, 2);
                     {
                        setState(61);
                        expression();
                    }
                    break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class AssignmentContext extends ParserRuleContext {

        public Constant_expressionContext constant_expression() {
            return getRuleContext(Constant_expressionContext.class, 0);
        }

        public TerminalNode ASSIGNMENT_OPERATOR() {
            return getToken(LodgeParser.ASSIGNMENT_OPERATOR, 0);
        }

        public ExpressionContext expression() {
            return getRuleContext(ExpressionContext.class, 0);
        }

        public Identifier_annotationContext identifier_annotation() {
            return getRuleContext(Identifier_annotationContext.class, 0);
        }

        public Type_annotationContext type_annotation() {
            return getRuleContext(Type_annotationContext.class, 0);
        }

        public AssignmentContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_assignment;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterAssignment(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitAssignment(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitAssignment(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final AssignmentContext assignment() throws RecognitionException {
        AssignmentContext _localctx = new AssignmentContext(_ctx, getState());
        enterRule(_localctx, 6, RULE_assignment);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(65);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__4) {
                    {
                        setState(64);
                        identifier_annotation();
                    }
                }

                setState(67);
                constant_expression();
                setState(69);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__4) {
                    {
                        setState(68);
                        type_annotation();
                    }
                }

                setState(71);
                match(ASSIGNMENT_OPERATOR);
                setState(72);
                expression();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Identifier_annotationContext extends ParserRuleContext {

        public AnnotationContext annotation() {
            return getRuleContext(AnnotationContext.class, 0);
        }

        public Identifier_annotationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_identifier_annotation;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterIdentifier_annotation(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitIdentifier_annotation(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitIdentifier_annotation(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final Identifier_annotationContext identifier_annotation() throws RecognitionException {
        Identifier_annotationContext _localctx = new Identifier_annotationContext(_ctx, getState());
        enterRule(_localctx, 8, RULE_identifier_annotation);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(74);
                annotation();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Type_annotationContext extends ParserRuleContext {

        public AnnotationContext annotation() {
            return getRuleContext(AnnotationContext.class, 0);
        }

        public Type_annotationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_type_annotation;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterType_annotation(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitType_annotation(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitType_annotation(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final Type_annotationContext type_annotation() throws RecognitionException {
        Type_annotationContext _localctx = new Type_annotationContext(_ctx, getState());
        enterRule(_localctx, 10, RULE_type_annotation);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(76);
                annotation();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Constant_expressionContext extends ParserRuleContext {

        public List<ValueContext> value() {
            return getRuleContexts(ValueContext.class);
        }

        public ValueContext value(int i) {
            return getRuleContext(ValueContext.class, i);
        }

        public Constant_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_constant_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterConstant_expression(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitConstant_expression(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitConstant_expression(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final Constant_expressionContext constant_expression() throws RecognitionException {
        Constant_expressionContext _localctx = new Constant_expressionContext(_ctx, getState());
        enterRule(_localctx, 12, RULE_constant_expression);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(79);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        {
                            setState(78);
                            value();
                        }
                    }
                    setState(81);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                } while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__2) | (1L << BOOLEAN) | (1L << UNDEFINED) | (1L << COMMA) | (1L << NUMBER) | (1L << CHARACTER) | (1L << STRING) | (1L << QUOTELESS_STRING))) != 0));
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ExpressionContext extends ParserRuleContext {

        public List<ValueContext> value() {
            return getRuleContexts(ValueContext.class);
        }

        public ValueContext value(int i) {
            return getRuleContext(ValueContext.class, i);
        }

        public List<FunctionContext> function() {
            return getRuleContexts(FunctionContext.class);
        }

        public FunctionContext function(int i) {
            return getRuleContext(FunctionContext.class, i);
        }

        public List<ErrorContext> error() {
            return getRuleContexts(ErrorContext.class);
        }

        public ErrorContext error(int i) {
            return getRuleContext(ErrorContext.class, i);
        }

        public List<ConditionalContext> conditional() {
            return getRuleContexts(ConditionalContext.class);
        }

        public ConditionalContext conditional(int i) {
            return getRuleContext(ConditionalContext.class, i);
        }

        public ExpressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterExpression(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitExpression(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitExpression(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final ExpressionContext expression() throws RecognitionException {
        ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
        enterRule(_localctx, 14, RULE_expression);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(87);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        setState(87);
                        _errHandler.sync(this);
                        switch (_input.LA(1)) {
                            case T__0:
                            case T__2:
                            case BOOLEAN:
                            case UNDEFINED:
                            case COMMA:
                            case NUMBER:
                            case CHARACTER:
                            case STRING:
                            case QUOTELESS_STRING: {
                                setState(83);
                                value();
                            }
                            break;
                            case T__4:
                            case FUNCTION_START: {
                                setState(84);
                                function();
                            }
                            break;
                            case ERROR_START: {
                                setState(85);
                                error();
                            }
                            break;
                            case CONDITIONAL_START: {
                                setState(86);
                                conditional();
                            }
                            break;
                            default:
                                throw new NoViableAltException(this);
                        }
                    }
                    setState(89);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                } while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__2) | (1L << T__4) | (1L << FUNCTION_START) | (1L << ERROR_START) | (1L << CONDITIONAL_START) | (1L << BOOLEAN) | (1L << UNDEFINED) | (1L << COMMA) | (1L << NUMBER) | (1L << CHARACTER) | (1L << STRING) | (1L << QUOTELESS_STRING))) != 0));
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ValueContext extends ParserRuleContext {

        public ValueContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_value;
        }

        public ValueContext() {
        }

        public void copyFrom(ValueContext ctx) {
            super.copyFrom(ctx);
        }
    }

    public static class TupleContext extends ValueContext {

        public ExpressionsContext expressions() {
            return getRuleContext(ExpressionsContext.class, 0);
        }

        public TerminalNode NEWLINE() {
            return getToken(LodgeParser.NEWLINE, 0);
        }

        public SeparatorContext separator() {
            return getRuleContext(SeparatorContext.class, 0);
        }

        public TupleContext(ValueContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterTuple(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitTuple(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitTuple(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public static class NumberContext extends ValueContext {

        public TerminalNode NUMBER() {
            return getToken(LodgeParser.NUMBER, 0);
        }

        public NumberContext(ValueContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterNumber(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitNumber(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitNumber(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public static class CharacterContext extends ValueContext {

        public TerminalNode CHARACTER() {
            return getToken(LodgeParser.CHARACTER, 0);
        }

        public CharacterContext(ValueContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterCharacter(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitCharacter(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitCharacter(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public static class CommaContext extends ValueContext {

        public TerminalNode COMMA() {
            return getToken(LodgeParser.COMMA, 0);
        }

        public CommaContext(ValueContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterComma(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitComma(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitComma(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public static class Quoteless_stringContext extends ValueContext {

        public TerminalNode QUOTELESS_STRING() {
            return getToken(LodgeParser.QUOTELESS_STRING, 0);
        }

        public Quoteless_stringContext(ValueContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterQuoteless_string(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitQuoteless_string(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitQuoteless_string(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public static class SetContext extends ValueContext {

        public ExpressionsContext expressions() {
            return getRuleContext(ExpressionsContext.class, 0);
        }

        public TerminalNode NEWLINE() {
            return getToken(LodgeParser.NEWLINE, 0);
        }

        public SeparatorContext separator() {
            return getRuleContext(SeparatorContext.class, 0);
        }

        public SetContext(ValueContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterSet(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitSet(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitSet(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public static class BooleanContext extends ValueContext {

        public TerminalNode BOOLEAN() {
            return getToken(LodgeParser.BOOLEAN, 0);
        }

        public BooleanContext(ValueContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterBoolean(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitBoolean(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitBoolean(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public static class StringContext extends ValueContext {

        public TerminalNode STRING() {
            return getToken(LodgeParser.STRING, 0);
        }

        public StringContext(ValueContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterString(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitString(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitString(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public static class Empty_tupleContext extends ValueContext {

        public Empty_tupleContext(ValueContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterEmpty_tuple(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitEmpty_tuple(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitEmpty_tuple(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public static class Empty_mapContext extends ValueContext {

        public Empty_mapContext(ValueContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterEmpty_map(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitEmpty_map(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitEmpty_map(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public static class MapContext extends ValueContext {

        public AssignmentsContext assignments() {
            return getRuleContext(AssignmentsContext.class, 0);
        }

        public TerminalNode NEWLINE() {
            return getToken(LodgeParser.NEWLINE, 0);
        }

        public SeparatorContext separator() {
            return getRuleContext(SeparatorContext.class, 0);
        }

        public MapContext(ValueContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterMap(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitMap(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitMap(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public static class UndefinedContext extends ValueContext {

        public TerminalNode UNDEFINED() {
            return getToken(LodgeParser.UNDEFINED, 0);
        }

        public UndefinedContext(ValueContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterUndefined(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitUndefined(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitUndefined(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final ValueContext value() throws RecognitionException {
        ValueContext _localctx = new ValueContext(_ctx, getState());
        enterRule(_localctx, 16, RULE_value);
        int _la;
        try {
            setState(132);
            _errHandler.sync(this);
            switch (getInterpreter().adaptivePredict(_input, 16, _ctx)) {
                case 1:
                    _localctx = new MapContext(_localctx);
                    enterOuterAlt(_localctx, 1);
                     {
                        setState(91);
                        match(T__0);
                        setState(93);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                        if (_la == NEWLINE) {
                            {
                                setState(92);
                                match(NEWLINE);
                            }
                        }

                        setState(95);
                        assignments();
                        setState(97);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                        if (_la == BLOCK_END || _la == NEWLINE) {
                            {
                                setState(96);
                                separator();
                            }
                        }

                        setState(99);
                        match(T__1);
                    }
                    break;
                case 2:
                    _localctx = new SetContext(_localctx);
                    enterOuterAlt(_localctx, 2);
                     {
                        setState(101);
                        match(T__0);
                        setState(103);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                        if (_la == NEWLINE) {
                            {
                                setState(102);
                                match(NEWLINE);
                            }
                        }

                        setState(105);
                        expressions();
                        setState(107);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                        if (_la == BLOCK_END || _la == NEWLINE) {
                            {
                                setState(106);
                                separator();
                            }
                        }

                        setState(109);
                        match(T__1);
                    }
                    break;
                case 3:
                    _localctx = new Empty_mapContext(_localctx);
                    enterOuterAlt(_localctx, 3);
                     {
                        setState(111);
                        match(T__0);
                        setState(112);
                        match(T__1);
                    }
                    break;
                case 4:
                    _localctx = new TupleContext(_localctx);
                    enterOuterAlt(_localctx, 4);
                     {
                        setState(113);
                        match(T__2);
                        setState(115);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                        if (_la == NEWLINE) {
                            {
                                setState(114);
                                match(NEWLINE);
                            }
                        }

                        setState(117);
                        expressions();
                        setState(119);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                        if (_la == BLOCK_END || _la == NEWLINE) {
                            {
                                setState(118);
                                separator();
                            }
                        }

                        setState(121);
                        match(T__3);
                    }
                    break;
                case 5:
                    _localctx = new Empty_tupleContext(_localctx);
                    enterOuterAlt(_localctx, 5);
                     {
                        setState(123);
                        match(T__2);
                        setState(124);
                        match(T__3);
                    }
                    break;
                case 6:
                    _localctx = new NumberContext(_localctx);
                    enterOuterAlt(_localctx, 6);
                     {
                        setState(125);
                        match(NUMBER);
                    }
                    break;
                case 7:
                    _localctx = new BooleanContext(_localctx);
                    enterOuterAlt(_localctx, 7);
                     {
                        setState(126);
                        match(BOOLEAN);
                    }
                    break;
                case 8:
                    _localctx = new UndefinedContext(_localctx);
                    enterOuterAlt(_localctx, 8);
                     {
                        setState(127);
                        match(UNDEFINED);
                    }
                    break;
                case 9:
                    _localctx = new CharacterContext(_localctx);
                    enterOuterAlt(_localctx, 9);
                     {
                        setState(128);
                        match(CHARACTER);
                    }
                    break;
                case 10:
                    _localctx = new StringContext(_localctx);
                    enterOuterAlt(_localctx, 10);
                     {
                        setState(129);
                        match(STRING);
                    }
                    break;
                case 11:
                    _localctx = new CommaContext(_localctx);
                    enterOuterAlt(_localctx, 11);
                     {
                        setState(130);
                        match(COMMA);
                    }
                    break;
                case 12:
                    _localctx = new Quoteless_stringContext(_localctx);
                    enterOuterAlt(_localctx, 12);
                     {
                        setState(131);
                        match(QUOTELESS_STRING);
                    }
                    break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class AnnotationContext extends ParserRuleContext {

        public ExpressionsContext expressions() {
            return getRuleContext(ExpressionsContext.class, 0);
        }

        public TerminalNode NEWLINE() {
            return getToken(LodgeParser.NEWLINE, 0);
        }

        public SeparatorContext separator() {
            return getRuleContext(SeparatorContext.class, 0);
        }

        public AnnotationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_annotation;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterAnnotation(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitAnnotation(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitAnnotation(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final AnnotationContext annotation() throws RecognitionException {
        AnnotationContext _localctx = new AnnotationContext(_ctx, getState());
        enterRule(_localctx, 18, RULE_annotation);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(134);
                match(T__4);
                setState(136);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == NEWLINE) {
                    {
                        setState(135);
                        match(NEWLINE);
                    }
                }

                setState(138);
                expressions();
                setState(140);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == BLOCK_END || _la == NEWLINE) {
                    {
                        setState(139);
                        separator();
                    }
                }

                setState(142);
                match(T__5);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class FunctionContext extends ParserRuleContext {

        public TerminalNode FUNCTION_START() {
            return getToken(LodgeParser.FUNCTION_START, 0);
        }

        public List<TerminalNode> BLOCK_END() {
            return getTokens(LodgeParser.BLOCK_END);
        }

        public TerminalNode BLOCK_END(int i) {
            return getToken(LodgeParser.BLOCK_END, i);
        }

        public StatementsContext statements() {
            return getRuleContext(StatementsContext.class, 0);
        }

        public Function_annotationContext function_annotation() {
            return getRuleContext(Function_annotationContext.class, 0);
        }

        public Constant_expressionContext constant_expression() {
            return getRuleContext(Constant_expressionContext.class, 0);
        }

        public FunctionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_function;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterFunction(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitFunction(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitFunction(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final FunctionContext function() throws RecognitionException {
        FunctionContext _localctx = new FunctionContext(_ctx, getState());
        enterRule(_localctx, 20, RULE_function);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(145);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__4) {
                    {
                        setState(144);
                        function_annotation();
                    }
                }

                setState(147);
                match(FUNCTION_START);
                setState(149);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__2) | (1L << BOOLEAN) | (1L << UNDEFINED) | (1L << COMMA) | (1L << NUMBER) | (1L << CHARACTER) | (1L << STRING) | (1L << QUOTELESS_STRING))) != 0)) {
                    {
                        setState(148);
                        constant_expression();
                    }
                }

                setState(151);
                match(BLOCK_END);
                setState(152);
                statements();
                setState(153);
                match(BLOCK_END);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Function_annotationContext extends ParserRuleContext {

        public AnnotationContext annotation() {
            return getRuleContext(AnnotationContext.class, 0);
        }

        public Function_annotationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_function_annotation;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterFunction_annotation(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitFunction_annotation(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitFunction_annotation(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final Function_annotationContext function_annotation() throws RecognitionException {
        Function_annotationContext _localctx = new Function_annotationContext(_ctx, getState());
        enterRule(_localctx, 22, RULE_function_annotation);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(155);
                annotation();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ErrorContext extends ParserRuleContext {

        public TerminalNode ERROR_START() {
            return getToken(LodgeParser.ERROR_START, 0);
        }

        public List<ExpressionContext> expression() {
            return getRuleContexts(ExpressionContext.class);
        }

        public ExpressionContext expression(int i) {
            return getRuleContext(ExpressionContext.class, i);
        }

        public List<TerminalNode> BLOCK_END() {
            return getTokens(LodgeParser.BLOCK_END);
        }

        public TerminalNode BLOCK_END(int i) {
            return getToken(LodgeParser.BLOCK_END, i);
        }

        public ErrorContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_error;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterError(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitError(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitError(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final ErrorContext error() throws RecognitionException {
        ErrorContext _localctx = new ErrorContext(_ctx, getState());
        enterRule(_localctx, 24, RULE_error);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(157);
                match(ERROR_START);
                setState(158);
                expression();
                setState(159);
                match(BLOCK_END);
                setState(161);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__2) | (1L << T__4) | (1L << FUNCTION_START) | (1L << ERROR_START) | (1L << CONDITIONAL_START) | (1L << BOOLEAN) | (1L << UNDEFINED) | (1L << COMMA) | (1L << NUMBER) | (1L << CHARACTER) | (1L << STRING) | (1L << QUOTELESS_STRING))) != 0)) {
                    {
                        setState(160);
                        expression();
                    }
                }

                setState(163);
                match(BLOCK_END);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ConditionalContext extends ParserRuleContext {

        public TerminalNode CONDITIONAL_START() {
            return getToken(LodgeParser.CONDITIONAL_START, 0);
        }

        public List<StatementsContext> statements() {
            return getRuleContexts(StatementsContext.class);
        }

        public StatementsContext statements(int i) {
            return getRuleContext(StatementsContext.class, i);
        }

        public List<TerminalNode> BLOCK_END() {
            return getTokens(LodgeParser.BLOCK_END);
        }

        public TerminalNode BLOCK_END(int i) {
            return getToken(LodgeParser.BLOCK_END, i);
        }

        public ConditionalContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_conditional;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterConditional(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitConditional(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitConditional(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final ConditionalContext conditional() throws RecognitionException {
        ConditionalContext _localctx = new ConditionalContext(_ctx, getState());
        enterRule(_localctx, 26, RULE_conditional);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(165);
                match(CONDITIONAL_START);
                setState(166);
                statements();
                setState(167);
                match(BLOCK_END);
                setState(169);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__2) | (1L << T__4) | (1L << FUNCTION_START) | (1L << ERROR_START) | (1L << CONDITIONAL_START) | (1L << NEWLINE) | (1L << BOOLEAN) | (1L << UNDEFINED) | (1L << COMMA) | (1L << NUMBER) | (1L << CHARACTER) | (1L << STRING) | (1L << QUOTELESS_STRING))) != 0)) {
                    {
                        setState(168);
                        statements();
                    }
                }

                setState(171);
                match(BLOCK_END);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class AssignmentsContext extends ParserRuleContext {

        public List<AssignmentContext> assignment() {
            return getRuleContexts(AssignmentContext.class);
        }

        public AssignmentContext assignment(int i) {
            return getRuleContext(AssignmentContext.class, i);
        }

        public List<SeparatorContext> separator() {
            return getRuleContexts(SeparatorContext.class);
        }

        public SeparatorContext separator(int i) {
            return getRuleContext(SeparatorContext.class, i);
        }

        public AssignmentsContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_assignments;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterAssignments(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitAssignments(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitAssignments(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final AssignmentsContext assignments() throws RecognitionException {
        AssignmentsContext _localctx = new AssignmentsContext(_ctx, getState());
        enterRule(_localctx, 28, RULE_assignments);
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(173);
                assignment();
                setState(179);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 23, _ctx);
                while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                    if (_alt == 1) {
                        {
                            {
                                setState(174);
                                separator();
                                setState(175);
                                assignment();
                            }
                        }
                    }
                    setState(181);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 23, _ctx);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ExpressionsContext extends ParserRuleContext {

        public List<ExpressionContext> expression() {
            return getRuleContexts(ExpressionContext.class);
        }

        public ExpressionContext expression(int i) {
            return getRuleContext(ExpressionContext.class, i);
        }

        public List<SeparatorContext> separator() {
            return getRuleContexts(SeparatorContext.class);
        }

        public SeparatorContext separator(int i) {
            return getRuleContext(SeparatorContext.class, i);
        }

        public ExpressionsContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_expressions;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterExpressions(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitExpressions(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitExpressions(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final ExpressionsContext expressions() throws RecognitionException {
        ExpressionsContext _localctx = new ExpressionsContext(_ctx, getState());
        enterRule(_localctx, 30, RULE_expressions);
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(182);
                expression();
                setState(188);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 24, _ctx);
                while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                    if (_alt == 1) {
                        {
                            {
                                setState(183);
                                separator();
                                setState(184);
                                expression();
                            }
                        }
                    }
                    setState(190);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 24, _ctx);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class SeparatorContext extends ParserRuleContext {

        public TerminalNode BLOCK_END() {
            return getToken(LodgeParser.BLOCK_END, 0);
        }

        public TerminalNode NEWLINE() {
            return getToken(LodgeParser.NEWLINE, 0);
        }

        public SeparatorContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_separator;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).enterSeparator(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ILodgeListener) {
                ((ILodgeListener) listener).exitSeparator(this);
            }
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof ILodgeVisitor) {
                return ((ILodgeVisitor<? extends T>) visitor).visitSeparator(this);
            } else {
                return visitor.visitChildren(this);
            }
        }
    }

    public final SeparatorContext separator() throws RecognitionException {
        SeparatorContext _localctx = new SeparatorContext(_ctx, getState());
        enterRule(_localctx, 32, RULE_separator);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(191);
                _la = _input.LA(1);
                if (!(_la == BLOCK_END || _la == NEWLINE)) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF) {
                        //matchedEOF = true;
                    }
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static final String _serializedATN
        = "\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\30\u00c4\4\2\t\2"
        + "\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"
        + "\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"
        + "\3\2\3\2\3\3\7\3(\n\3\f\3\16\3+\13\3\3\3\3\3\6\3/\n\3\r\3\16\3\60\3\3"
        + "\7\3\64\n\3\f\3\16\3\67\13\3\3\3\7\3:\n\3\f\3\16\3=\13\3\3\4\3\4\5\4A"
        + "\n\4\3\5\5\5D\n\5\3\5\3\5\5\5H\n\5\3\5\3\5\3\5\3\6\3\6\3\7\3\7\3\b\6\b"
        + "R\n\b\r\b\16\bS\3\t\3\t\3\t\3\t\6\tZ\n\t\r\t\16\t[\3\n\3\n\5\n`\n\n\3"
        + "\n\3\n\5\nd\n\n\3\n\3\n\3\n\3\n\5\nj\n\n\3\n\3\n\5\nn\n\n\3\n\3\n\3\n"
        + "\3\n\3\n\3\n\5\nv\n\n\3\n\3\n\5\nz\n\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n"
        + "\3\n\3\n\3\n\5\n\u0087\n\n\3\13\3\13\5\13\u008b\n\13\3\13\3\13\5\13\u008f"
        + "\n\13\3\13\3\13\3\f\5\f\u0094\n\f\3\f\3\f\5\f\u0098\n\f\3\f\3\f\3\f\3"
        + "\f\3\r\3\r\3\16\3\16\3\16\3\16\5\16\u00a4\n\16\3\16\3\16\3\17\3\17\3\17"
        + "\3\17\5\17\u00ac\n\17\3\17\3\17\3\20\3\20\3\20\3\20\7\20\u00b4\n\20\f"
        + "\20\16\20\u00b7\13\20\3\21\3\21\3\21\3\21\7\21\u00bd\n\21\f\21\16\21\u00c0"
        + "\13\21\3\22\3\22\3\22\2\2\23\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \""
        + "\2\3\4\2\f\f\16\16\2\u00d7\2$\3\2\2\2\4)\3\2\2\2\6@\3\2\2\2\bC\3\2\2\2"
        + "\nL\3\2\2\2\fN\3\2\2\2\16Q\3\2\2\2\20Y\3\2\2\2\22\u0086\3\2\2\2\24\u0088"
        + "\3\2\2\2\26\u0093\3\2\2\2\30\u009d\3\2\2\2\32\u009f\3\2\2\2\34\u00a7\3"
        + "\2\2\2\36\u00af\3\2\2\2 \u00b8\3\2\2\2\"\u00c1\3\2\2\2$%\5\4\3\2%\3\3"
        + "\2\2\2&(\7\16\2\2\'&\3\2\2\2(+\3\2\2\2)\'\3\2\2\2)*\3\2\2\2*,\3\2\2\2"
        + "+)\3\2\2\2,\65\5\6\4\2-/\7\16\2\2.-\3\2\2\2/\60\3\2\2\2\60.\3\2\2\2\60"
        + "\61\3\2\2\2\61\62\3\2\2\2\62\64\5\6\4\2\63.\3\2\2\2\64\67\3\2\2\2\65\63"
        + "\3\2\2\2\65\66\3\2\2\2\66;\3\2\2\2\67\65\3\2\2\28:\7\16\2\298\3\2\2\2"
        + ":=\3\2\2\2;9\3\2\2\2;<\3\2\2\2<\5\3\2\2\2=;\3\2\2\2>A\5\b\5\2?A\5\20\t"
        + "\2@>\3\2\2\2@?\3\2\2\2A\7\3\2\2\2BD\5\n\6\2CB\3\2\2\2CD\3\2\2\2DE\3\2"
        + "\2\2EG\5\16\b\2FH\5\f\7\2GF\3\2\2\2GH\3\2\2\2HI\3\2\2\2IJ\7\r\2\2JK\5"
        + "\20\t\2K\t\3\2\2\2LM\5\24\13\2M\13\3\2\2\2NO\5\24\13\2O\r\3\2\2\2PR\5"
        + "\22\n\2QP\3\2\2\2RS\3\2\2\2SQ\3\2\2\2ST\3\2\2\2T\17\3\2\2\2UZ\5\22\n\2"
        + "VZ\5\26\f\2WZ\5\32\16\2XZ\5\34\17\2YU\3\2\2\2YV\3\2\2\2YW\3\2\2\2YX\3"
        + "\2\2\2Z[\3\2\2\2[Y\3\2\2\2[\\\3\2\2\2\\\21\3\2\2\2]_\7\3\2\2^`\7\16\2"
        + "\2_^\3\2\2\2_`\3\2\2\2`a\3\2\2\2ac\5\36\20\2bd\5\"\22\2cb\3\2\2\2cd\3"
        + "\2\2\2de\3\2\2\2ef\7\4\2\2f\u0087\3\2\2\2gi\7\3\2\2hj\7\16\2\2ih\3\2\2"
        + "\2ij\3\2\2\2jk\3\2\2\2km\5 \21\2ln\5\"\22\2ml\3\2\2\2mn\3\2\2\2no\3\2"
        + "\2\2op\7\4\2\2p\u0087\3\2\2\2qr\7\3\2\2r\u0087\7\4\2\2su\7\5\2\2tv\7\16"
        + "\2\2ut\3\2\2\2uv\3\2\2\2vw\3\2\2\2wy\5 \21\2xz\5\"\22\2yx\3\2\2\2yz\3"
        + "\2\2\2z{\3\2\2\2{|\7\6\2\2|\u0087\3\2\2\2}~\7\5\2\2~\u0087\7\6\2\2\177"
        + "\u0087\7\22\2\2\u0080\u0087\7\17\2\2\u0081\u0087\7\20\2\2\u0082\u0087"
        + "\7\25\2\2\u0083\u0087\7\26\2\2\u0084\u0087\7\21\2\2\u0085\u0087\7\27\2"
        + "\2\u0086]\3\2\2\2\u0086g\3\2\2\2\u0086q\3\2\2\2\u0086s\3\2\2\2\u0086}"
        + "\3\2\2\2\u0086\177\3\2\2\2\u0086\u0080\3\2\2\2\u0086\u0081\3\2\2\2\u0086"
        + "\u0082\3\2\2\2\u0086\u0083\3\2\2\2\u0086\u0084\3\2\2\2\u0086\u0085\3\2"
        + "\2\2\u0087\23\3\2\2\2\u0088\u008a\7\7\2\2\u0089\u008b\7\16\2\2\u008a\u0089"
        + "\3\2\2\2\u008a\u008b\3\2\2\2\u008b\u008c\3\2\2\2\u008c\u008e\5 \21\2\u008d"
        + "\u008f\5\"\22\2\u008e\u008d\3\2\2\2\u008e\u008f\3\2\2\2\u008f\u0090\3"
        + "\2\2\2\u0090\u0091\7\b\2\2\u0091\25\3\2\2\2\u0092\u0094\5\30\r\2\u0093"
        + "\u0092\3\2\2\2\u0093\u0094\3\2\2\2\u0094\u0095\3\2\2\2\u0095\u0097\7\t"
        + "\2\2\u0096\u0098\5\16\b\2\u0097\u0096\3\2\2\2\u0097\u0098\3\2\2\2\u0098"
        + "\u0099\3\2\2\2\u0099\u009a\7\f\2\2\u009a\u009b\5\4\3\2\u009b\u009c\7\f"
        + "\2\2\u009c\27\3\2\2\2\u009d\u009e\5\24\13\2\u009e\31\3\2\2\2\u009f\u00a0"
        + "\7\n\2\2\u00a0\u00a1\5\20\t\2\u00a1\u00a3\7\f\2\2\u00a2\u00a4\5\20\t\2"
        + "\u00a3\u00a2\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4\u00a5\3\2\2\2\u00a5\u00a6"
        + "\7\f\2\2\u00a6\33\3\2\2\2\u00a7\u00a8\7\13\2\2\u00a8\u00a9\5\4\3\2\u00a9"
        + "\u00ab\7\f\2\2\u00aa\u00ac\5\4\3\2\u00ab\u00aa\3\2\2\2\u00ab\u00ac\3\2"
        + "\2\2\u00ac\u00ad\3\2\2\2\u00ad\u00ae\7\f\2\2\u00ae\35\3\2\2\2\u00af\u00b5"
        + "\5\b\5\2\u00b0\u00b1\5\"\22\2\u00b1\u00b2\5\b\5\2\u00b2\u00b4\3\2\2\2"
        + "\u00b3\u00b0\3\2\2\2\u00b4\u00b7\3\2\2\2\u00b5\u00b3\3\2\2\2\u00b5\u00b6"
        + "\3\2\2\2\u00b6\37\3\2\2\2\u00b7\u00b5\3\2\2\2\u00b8\u00be\5\20\t\2\u00b9"
        + "\u00ba\5\"\22\2\u00ba\u00bb\5\20\t\2\u00bb\u00bd\3\2\2\2\u00bc\u00b9\3"
        + "\2\2\2\u00bd\u00c0\3\2\2\2\u00be\u00bc\3\2\2\2\u00be\u00bf\3\2\2\2\u00bf"
        + "!\3\2\2\2\u00c0\u00be\3\2\2\2\u00c1\u00c2\t\2\2\2\u00c2#\3\2\2\2\33)\60"
        + "\65;@CGSY[_cimuy\u0086\u008a\u008e\u0093\u0097\u00a3\u00ab\u00b5\u00be";
    public static final ATN _ATN
        = new ATNDeserializer().deserialize(_serializedATN.toCharArray());

    static {
        _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
        for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
            _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
        }
    }
}
