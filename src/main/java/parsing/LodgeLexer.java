package parsing;

// Generated from Lodge.g4 by ANTLR 4.7.1

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class LodgeLexer extends Lexer {

    static {
        RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION);
    }

    protected static final DFA[] _decisionToDFA;
    protected static final PredictionContextCache _sharedContextCache
        = new PredictionContextCache();
    public static final int T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, FUNCTION_START = 7, ERROR_START = 8,
        CONDITIONAL_START = 9, BLOCK_END = 10, ASSIGNMENT_OPERATOR = 11, NEWLINE = 12,
        BOOLEAN = 13, UNDEFINED = 14, COMMA = 15, NUMBER = 16, UNSIGNED_NUMBER = 17, REAL = 18,
        CHARACTER = 19, STRING = 20, QUOTELESS_STRING = 21, TRASH = 22;
    public static String[] channelNames = {
        "DEFAULT_TOKEN_CHANNEL", "HIDDEN"
    };

    public static String[] modeNames = {
        "DEFAULT_MODE"
    };

    public static final String[] ruleNames = {
        "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "FUNCTION_START", "ERROR_START",
        "CONDITIONAL_START", "BLOCK_END", "ASSIGNMENT_OPERATOR", "NEWLINE", "BOOLEAN",
        "UNDEFINED", "COMMA", "NUMBER", "UNSIGNED_NUMBER", "REAL", "INTEGER",
        "EXPONENT", "CHARACTER", "STRING", "ESCAPE_CHARACTER", "UNICODE", "HEX",
        "QUOTELESS_STRING", "WHITESPACE", "COMMENT", "TRASH"
    };

    private static final String[] _LITERAL_NAMES = {
        null, "'{'", "'}'", "'['", "']'", "'('", "')'", "'\\'", "'!'", "'?'",
        "'|'", null, null, null, "'undefined'", "','"
    };
    private static final String[] _SYMBOLIC_NAMES = {
        null, null, null, null, null, null, null, "FUNCTION_START", "ERROR_START",
        "CONDITIONAL_START", "BLOCK_END", "ASSIGNMENT_OPERATOR", "NEWLINE", "BOOLEAN",
        "UNDEFINED", "COMMA", "NUMBER", "UNSIGNED_NUMBER", "REAL", "CHARACTER",
        "STRING", "QUOTELESS_STRING", "TRASH"
    };
    public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

    /**
     * @deprecated Use {@link #VOCABULARY} instead.
     */
    @Deprecated
    public static final String[] tokenNames;

    static {
        tokenNames = new String[_SYMBOLIC_NAMES.length];
        for (int i = 0; i < tokenNames.length; i++) {
            tokenNames[i] = VOCABULARY.getLiteralName(i);
            if (tokenNames[i] == null) {
                tokenNames[i] = VOCABULARY.getSymbolicName(i);
            }

            if (tokenNames[i] == null) {
                tokenNames[i] = "<INVALID>";
            }
        }
    }

    @Override
    @Deprecated
    public String[] getTokenNames() {
        return tokenNames;
    }

    @Override

    public Vocabulary getVocabulary() {
        return VOCABULARY;
    }

    public LodgeLexer(CharStream input) {
        super(input);
        _interp = new LexerATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
    }

    @Override
    public String getGrammarFileName() {
        return "Lodge.g4";
    }

    @Override
    public String[] getRuleNames() {
        return ruleNames;
    }

    @Override
    public String getSerializedATN() {
        return _serializedATN;
    }

    public String[] getChannelNames() {
        return channelNames;
    }

    @Override
    public String[] getModeNames() {
        return modeNames;
    }

    @Override
    public ATN getATN() {
        return _ATN;
    }

    public static final String _serializedATN
        = "\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\30\u00ca\b\1\4\2"
        + "\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"
        + "\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"
        + "\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"
        + "\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\3\2\3\2\3\3\3"
        + "\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3"
        + "\f\3\f\3\f\5\fU\n\f\3\r\3\r\3\r\5\rZ\n\r\3\16\3\16\3\16\3\16\3\16\5\16"
        + "a\n\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\21"
        + "\5\21p\n\21\3\21\3\21\3\22\3\22\3\23\3\23\3\23\6\23y\n\23\r\23\16\23z"
        + "\5\23}\n\23\3\23\5\23\u0080\n\23\3\24\3\24\3\24\7\24\u0085\n\24\f\24\16"
        + "\24\u0088\13\24\5\24\u008a\n\24\3\25\3\25\5\25\u008e\n\25\3\25\3\25\3"
        + "\26\3\26\3\26\3\26\3\26\5\26\u0097\n\26\3\26\3\26\3\27\3\27\3\27\3\27"
        + "\3\27\7\27\u00a0\n\27\f\27\16\27\u00a3\13\27\3\27\3\27\3\30\3\30\3\30"
        + "\5\30\u00aa\n\30\3\31\3\31\6\31\u00ae\n\31\r\31\16\31\u00af\3\32\3\32"
        + "\3\33\6\33\u00b5\n\33\r\33\16\33\u00b6\3\34\6\34\u00ba\n\34\r\34\16\34"
        + "\u00bb\3\35\3\35\7\35\u00c0\n\35\f\35\16\35\u00c3\13\35\3\36\3\36\5\36"
        + "\u00c7\n\36\3\36\3\36\2\2\37\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13"
        + "\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\2)\2+\25-\26/\2\61\2"
        + "\63\2\65\27\67\29\2;\30\3\2\r\4\2--//\3\2\62;\3\2\63;\4\2GGgg\6\2\f\f"
        + "\16\17))^^\6\2\f\f\16\17$$^^\t\2\61\61^^ddhhppttvv\5\2\62;CHch\13\2\13"
        + "\f\16\17\"%)+..<<AA]_}\177\4\2\13\13\"\"\4\2\f\f\16\17\2\u00d7\2\3\3\2"
        + "\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17"
        + "\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2"
        + "\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3"
        + "\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2\65\3\2\2\2\2;\3\2\2\2\3=\3\2\2\2\5?\3\2"
        + "\2\2\7A\3\2\2\2\tC\3\2\2\2\13E\3\2\2\2\rG\3\2\2\2\17I\3\2\2\2\21K\3\2"
        + "\2\2\23M\3\2\2\2\25O\3\2\2\2\27T\3\2\2\2\31Y\3\2\2\2\33`\3\2\2\2\35b\3"
        + "\2\2\2\37l\3\2\2\2!o\3\2\2\2#s\3\2\2\2%u\3\2\2\2\'\u0089\3\2\2\2)\u008b"
        + "\3\2\2\2+\u0091\3\2\2\2-\u009a\3\2\2\2/\u00a6\3\2\2\2\61\u00ab\3\2\2\2"
        + "\63\u00b1\3\2\2\2\65\u00b4\3\2\2\2\67\u00b9\3\2\2\29\u00bd\3\2\2\2;\u00c6"
        + "\3\2\2\2=>\7}\2\2>\4\3\2\2\2?@\7\177\2\2@\6\3\2\2\2AB\7]\2\2B\b\3\2\2"
        + "\2CD\7_\2\2D\n\3\2\2\2EF\7*\2\2F\f\3\2\2\2GH\7+\2\2H\16\3\2\2\2IJ\7^\2"
        + "\2J\20\3\2\2\2KL\7#\2\2L\22\3\2\2\2MN\7A\2\2N\24\3\2\2\2OP\7~\2\2P\26"
        + "\3\2\2\2QR\7<\2\2RU\7<\2\2SU\7<\2\2TQ\3\2\2\2TS\3\2\2\2U\30\3\2\2\2VW"
        + "\7\17\2\2WZ\7\f\2\2XZ\7\f\2\2YV\3\2\2\2YX\3\2\2\2Z\32\3\2\2\2[\\\7{\2"
        + "\2\\]\7g\2\2]a\7u\2\2^_\7p\2\2_a\7q\2\2`[\3\2\2\2`^\3\2\2\2a\34\3\2\2"
        + "\2bc\7w\2\2cd\7p\2\2de\7f\2\2ef\7g\2\2fg\7h\2\2gh\7k\2\2hi\7p\2\2ij\7"
        + "g\2\2jk\7f\2\2k\36\3\2\2\2lm\7.\2\2m \3\2\2\2np\t\2\2\2on\3\2\2\2op\3"
        + "\2\2\2pq\3\2\2\2qr\5#\22\2r\"\3\2\2\2st\5%\23\2t$\3\2\2\2u|\5\'\24\2v"
        + "x\7\60\2\2wy\t\3\2\2xw\3\2\2\2yz\3\2\2\2zx\3\2\2\2z{\3\2\2\2{}\3\2\2\2"
        + "|v\3\2\2\2|}\3\2\2\2}\177\3\2\2\2~\u0080\5)\25\2\177~\3\2\2\2\177\u0080"
        + "\3\2\2\2\u0080&\3\2\2\2\u0081\u008a\7\62\2\2\u0082\u0086\t\4\2\2\u0083"
        + "\u0085\t\3\2\2\u0084\u0083\3\2\2\2\u0085\u0088\3\2\2\2\u0086\u0084\3\2"
        + "\2\2\u0086\u0087\3\2\2\2\u0087\u008a\3\2\2\2\u0088\u0086\3\2\2\2\u0089"
        + "\u0081\3\2\2\2\u0089\u0082\3\2\2\2\u008a(\3\2\2\2\u008b\u008d\t\5\2\2"
        + "\u008c\u008e\t\2\2\2\u008d\u008c\3\2\2\2\u008d\u008e\3\2\2\2\u008e\u008f"
        + "\3\2\2\2\u008f\u0090\5\'\24\2\u0090*\3\2\2\2\u0091\u0096\7)\2\2\u0092"
        + "\u0097\5/\30\2\u0093\u0094\7^\2\2\u0094\u0097\7)\2\2\u0095\u0097\n\6\2"
        + "\2\u0096\u0092\3\2\2\2\u0096\u0093\3\2\2\2\u0096\u0095\3\2\2\2\u0097\u0098"
        + "\3\2\2\2\u0098\u0099\7)\2\2\u0099,\3\2\2\2\u009a\u00a1\7$\2\2\u009b\u00a0"
        + "\5/\30\2\u009c\u009d\7^\2\2\u009d\u00a0\7$\2\2\u009e\u00a0\n\7\2\2\u009f"
        + "\u009b\3\2\2\2\u009f\u009c\3\2\2\2\u009f\u009e\3\2\2\2\u00a0\u00a3\3\2"
        + "\2\2\u00a1\u009f\3\2\2\2\u00a1\u00a2\3\2\2\2\u00a2\u00a4\3\2\2\2\u00a3"
        + "\u00a1\3\2\2\2\u00a4\u00a5\7$\2\2\u00a5.\3\2\2\2\u00a6\u00a9\7^\2\2\u00a7"
        + "\u00aa\t\b\2\2\u00a8\u00aa\5\61\31\2\u00a9\u00a7\3\2\2\2\u00a9\u00a8\3"
        + "\2\2\2\u00aa\60\3\2\2\2\u00ab\u00ad\7w\2\2\u00ac\u00ae\5\63\32\2\u00ad"
        + "\u00ac\3\2\2\2\u00ae\u00af\3\2\2\2\u00af\u00ad\3\2\2\2\u00af\u00b0\3\2"
        + "\2\2\u00b0\62\3\2\2\2\u00b1\u00b2\t\t\2\2\u00b2\64\3\2\2\2\u00b3\u00b5"
        + "\n\n\2\2\u00b4\u00b3\3\2\2\2\u00b5\u00b6\3\2\2\2\u00b6\u00b4\3\2\2\2\u00b6"
        + "\u00b7\3\2\2\2\u00b7\66\3\2\2\2\u00b8\u00ba\t\13\2\2\u00b9\u00b8\3\2\2"
        + "\2\u00ba\u00bb\3\2\2\2\u00bb\u00b9\3\2\2\2\u00bb\u00bc\3\2\2\2\u00bc8"
        + "\3\2\2\2\u00bd\u00c1\7%\2\2\u00be\u00c0\n\f\2\2\u00bf\u00be\3\2\2\2\u00c0"
        + "\u00c3\3\2\2\2\u00c1\u00bf\3\2\2\2\u00c1\u00c2\3\2\2\2\u00c2:\3\2\2\2"
        + "\u00c3\u00c1\3\2\2\2\u00c4\u00c7\5\67\34\2\u00c5\u00c7\59\35\2\u00c6\u00c4"
        + "\3\2\2\2\u00c6\u00c5\3\2\2\2\u00c7\u00c8\3\2\2\2\u00c8\u00c9\b\36\2\2"
        + "\u00c9<\3\2\2\2\26\2TY`oz|\177\u0086\u0089\u008d\u0096\u009f\u00a1\u00a9"
        + "\u00af\u00b6\u00bb\u00c1\u00c6\3\b\2\2";
    public static final ATN _ATN
        = new ATNDeserializer().deserialize(_serializedATN.toCharArray());

    static {
        _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
        for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
            _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
        }
    }
}
