package listening;

// Generated from Lodge.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.tree.ParseTreeListener;
import parsing.LodgeParser;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link LodgeParser}.
 */
public interface ILodgeListener extends ParseTreeListener {

    /**
     * Enter a parse tree produced by {@link LodgeParser#lodge}.
     *
     * @param ctx the parse tree
     */
    void enterLodge(LodgeParser.LodgeContext ctx);

    /**
     * Exit a parse tree produced by {@link LodgeParser#lodge}.
     *
     * @param ctx the parse tree
     */
    void exitLodge(LodgeParser.LodgeContext ctx);

    /**
     * Enter a parse tree produced by {@link LodgeParser#statements}.
     *
     * @param ctx the parse tree
     */
    void enterStatements(LodgeParser.StatementsContext ctx);

    /**
     * Exit a parse tree produced by {@link LodgeParser#statements}.
     *
     * @param ctx the parse tree
     */
    void exitStatements(LodgeParser.StatementsContext ctx);

    /**
     * Enter a parse tree produced by {@link LodgeParser#statement}.
     *
     * @param ctx the parse tree
     */
    void enterStatement(LodgeParser.StatementContext ctx);

    /**
     * Exit a parse tree produced by {@link LodgeParser#statement}.
     *
     * @param ctx the parse tree
     */
    void exitStatement(LodgeParser.StatementContext ctx);

    /**
     * Enter a parse tree produced by {@link LodgeParser#assignment}.
     *
     * @param ctx the parse tree
     */
    void enterAssignment(LodgeParser.AssignmentContext ctx);

    /**
     * Exit a parse tree produced by {@link LodgeParser#assignment}.
     *
     * @param ctx the parse tree
     */
    void exitAssignment(LodgeParser.AssignmentContext ctx);

    /**
     * Enter a parse tree produced by {@link LodgeParser#identifier_annotation}.
     *
     * @param ctx the parse tree
     */
    void enterIdentifier_annotation(LodgeParser.Identifier_annotationContext ctx);

    /**
     * Exit a parse tree produced by {@link LodgeParser#identifier_annotation}.
     *
     * @param ctx the parse tree
     */
    void exitIdentifier_annotation(LodgeParser.Identifier_annotationContext ctx);

    /**
     * Enter a parse tree produced by {@link LodgeParser#type_annotation}.
     *
     * @param ctx the parse tree
     */
    void enterType_annotation(LodgeParser.Type_annotationContext ctx);

    /**
     * Exit a parse tree produced by {@link LodgeParser#type_annotation}.
     *
     * @param ctx the parse tree
     */
    void exitType_annotation(LodgeParser.Type_annotationContext ctx);

    /**
     * Enter a parse tree produced by {@link LodgeParser#constant_expression}.
     *
     * @param ctx the parse tree
     */
    void enterConstant_expression(LodgeParser.Constant_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link LodgeParser#constant_expression}.
     *
     * @param ctx the parse tree
     */
    void exitConstant_expression(LodgeParser.Constant_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link LodgeParser#expression}.
     *
     * @param ctx the parse tree
     */
    void enterExpression(LodgeParser.ExpressionContext ctx);

    /**
     * Exit a parse tree produced by {@link LodgeParser#expression}.
     *
     * @param ctx the parse tree
     */
    void exitExpression(LodgeParser.ExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code map} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void enterMap(LodgeParser.MapContext ctx);

    /**
     * Exit a parse tree produced by the {@code map} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void exitMap(LodgeParser.MapContext ctx);

    /**
     * Enter a parse tree produced by the {@code set} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void enterSet(LodgeParser.SetContext ctx);

    /**
     * Exit a parse tree produced by the {@code set} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void exitSet(LodgeParser.SetContext ctx);

    /**
     * Enter a parse tree produced by the {@code empty_map} labeled alternative
     * in {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void enterEmpty_map(LodgeParser.Empty_mapContext ctx);

    /**
     * Exit a parse tree produced by the {@code empty_map} labeled alternative
     * in {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void exitEmpty_map(LodgeParser.Empty_mapContext ctx);

    /**
     * Enter a parse tree produced by the {@code tuple} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void enterTuple(LodgeParser.TupleContext ctx);

    /**
     * Exit a parse tree produced by the {@code tuple} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void exitTuple(LodgeParser.TupleContext ctx);

    /**
     * Enter a parse tree produced by the {@code empty_tuple} labeled
     * alternative in {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void enterEmpty_tuple(LodgeParser.Empty_tupleContext ctx);

    /**
     * Exit a parse tree produced by the {@code empty_tuple} labeled alternative
     * in {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void exitEmpty_tuple(LodgeParser.Empty_tupleContext ctx);

    /**
     * Enter a parse tree produced by the {@code number} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void enterNumber(LodgeParser.NumberContext ctx);

    /**
     * Exit a parse tree produced by the {@code number} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void exitNumber(LodgeParser.NumberContext ctx);

    /**
     * Enter a parse tree produced by the {@code boolean} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void enterBoolean(LodgeParser.BooleanContext ctx);

    /**
     * Exit a parse tree produced by the {@code boolean} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void exitBoolean(LodgeParser.BooleanContext ctx);

    /**
     * Enter a parse tree produced by the {@code undefined} labeled alternative
     * in {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void enterUndefined(LodgeParser.UndefinedContext ctx);

    /**
     * Exit a parse tree produced by the {@code undefined} labeled alternative
     * in {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void exitUndefined(LodgeParser.UndefinedContext ctx);

    /**
     * Enter a parse tree produced by the {@code character} labeled alternative
     * in {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void enterCharacter(LodgeParser.CharacterContext ctx);

    /**
     * Exit a parse tree produced by the {@code character} labeled alternative
     * in {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void exitCharacter(LodgeParser.CharacterContext ctx);

    /**
     * Enter a parse tree produced by the {@code string} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void enterString(LodgeParser.StringContext ctx);

    /**
     * Exit a parse tree produced by the {@code string} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void exitString(LodgeParser.StringContext ctx);

    /**
     * Enter a parse tree produced by the {@code comma} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void enterComma(LodgeParser.CommaContext ctx);

    /**
     * Exit a parse tree produced by the {@code comma} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void exitComma(LodgeParser.CommaContext ctx);

    /**
     * Enter a parse tree produced by the {@code quoteless_string} labeled
     * alternative in {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void enterQuoteless_string(LodgeParser.Quoteless_stringContext ctx);

    /**
     * Exit a parse tree produced by the {@code quoteless_string} labeled
     * alternative in {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     */
    void exitQuoteless_string(LodgeParser.Quoteless_stringContext ctx);

    /**
     * Enter a parse tree produced by {@link LodgeParser#annotation}.
     *
     * @param ctx the parse tree
     */
    void enterAnnotation(LodgeParser.AnnotationContext ctx);

    /**
     * Exit a parse tree produced by {@link LodgeParser#annotation}.
     *
     * @param ctx the parse tree
     */
    void exitAnnotation(LodgeParser.AnnotationContext ctx);

    /**
     * Enter a parse tree produced by {@link LodgeParser#function}.
     *
     * @param ctx the parse tree
     */
    void enterFunction(LodgeParser.FunctionContext ctx);

    /**
     * Exit a parse tree produced by {@link LodgeParser#function}.
     *
     * @param ctx the parse tree
     */
    void exitFunction(LodgeParser.FunctionContext ctx);

    /**
     * Enter a parse tree produced by {@link LodgeParser#function_annotation}.
     *
     * @param ctx the parse tree
     */
    void enterFunction_annotation(LodgeParser.Function_annotationContext ctx);

    /**
     * Exit a parse tree produced by {@link LodgeParser#function_annotation}.
     *
     * @param ctx the parse tree
     */
    void exitFunction_annotation(LodgeParser.Function_annotationContext ctx);

    /**
     * Enter a parse tree produced by {@link LodgeParser#error}.
     *
     * @param ctx the parse tree
     */
    void enterError(LodgeParser.ErrorContext ctx);

    /**
     * Exit a parse tree produced by {@link LodgeParser#error}.
     *
     * @param ctx the parse tree
     */
    void exitError(LodgeParser.ErrorContext ctx);

    /**
     * Enter a parse tree produced by {@link LodgeParser#conditional}.
     *
     * @param ctx the parse tree
     */
    void enterConditional(LodgeParser.ConditionalContext ctx);

    /**
     * Exit a parse tree produced by {@link LodgeParser#conditional}.
     *
     * @param ctx the parse tree
     */
    void exitConditional(LodgeParser.ConditionalContext ctx);

    /**
     * Enter a parse tree produced by {@link LodgeParser#assignments}.
     *
     * @param ctx the parse tree
     */
    void enterAssignments(LodgeParser.AssignmentsContext ctx);

    /**
     * Exit a parse tree produced by {@link LodgeParser#assignments}.
     *
     * @param ctx the parse tree
     */
    void exitAssignments(LodgeParser.AssignmentsContext ctx);

    /**
     * Enter a parse tree produced by {@link LodgeParser#expressions}.
     *
     * @param ctx the parse tree
     */
    void enterExpressions(LodgeParser.ExpressionsContext ctx);

    /**
     * Exit a parse tree produced by {@link LodgeParser#expressions}.
     *
     * @param ctx the parse tree
     */
    void exitExpressions(LodgeParser.ExpressionsContext ctx);

    /**
     * Enter a parse tree produced by {@link LodgeParser#separator}.
     *
     * @param ctx the parse tree
     */
    void enterSeparator(LodgeParser.SeparatorContext ctx);

    /**
     * Exit a parse tree produced by {@link LodgeParser#separator}.
     *
     * @param ctx the parse tree
     */
    void exitSeparator(LodgeParser.SeparatorContext ctx);
}
