package visiting;
// Generated from Lodge.g4 by ANTLR 4.7.1

import org.antlr.v4.runtime.tree.ParseTreeVisitor;
import parsing.LodgeParser;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link LodgeParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ILodgeVisitor<T> extends ParseTreeVisitor<T> {

    /**
     * Visit a parse tree produced by {@link LodgeParser#lodge}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLodge(LodgeParser.LodgeContext ctx);

    /**
     * Visit a parse tree produced by {@link LodgeParser#statements}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitStatements(LodgeParser.StatementsContext ctx);

    /**
     * Visit a parse tree produced by {@link LodgeParser#statement}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitStatement(LodgeParser.StatementContext ctx);

    /**
     * Visit a parse tree produced by {@link LodgeParser#assignment}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAssignment(LodgeParser.AssignmentContext ctx);

    /**
     * Visit a parse tree produced by {@link LodgeParser#identifier_annotation}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitIdentifier_annotation(LodgeParser.Identifier_annotationContext ctx);

    /**
     * Visit a parse tree produced by {@link LodgeParser#type_annotation}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitType_annotation(LodgeParser.Type_annotationContext ctx);

    /**
     * Visit a parse tree produced by {@link LodgeParser#constant_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitConstant_expression(LodgeParser.Constant_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link LodgeParser#expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitExpression(LodgeParser.ExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code map} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitMap(LodgeParser.MapContext ctx);

    /**
     * Visit a parse tree produced by the {@code set} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitSet(LodgeParser.SetContext ctx);

    /**
     * Visit a parse tree produced by the {@code empty_map} labeled alternative
     * in {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitEmpty_map(LodgeParser.Empty_mapContext ctx);

    /**
     * Visit a parse tree produced by the {@code tuple} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitTuple(LodgeParser.TupleContext ctx);

    /**
     * Visit a parse tree produced by the {@code empty_tuple} labeled
     * alternative in {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitEmpty_tuple(LodgeParser.Empty_tupleContext ctx);

    /**
     * Visit a parse tree produced by the {@code number} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitNumber(LodgeParser.NumberContext ctx);

    /**
     * Visit a parse tree produced by the {@code boolean} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBoolean(LodgeParser.BooleanContext ctx);

    /**
     * Visit a parse tree produced by the {@code undefined} labeled alternative
     * in {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUndefined(LodgeParser.UndefinedContext ctx);

    /**
     * Visit a parse tree produced by the {@code character} labeled alternative
     * in {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitCharacter(LodgeParser.CharacterContext ctx);

    /**
     * Visit a parse tree produced by the {@code string} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitString(LodgeParser.StringContext ctx);

    /**
     * Visit a parse tree produced by the {@code comma} labeled alternative in
     * {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitComma(LodgeParser.CommaContext ctx);

    /**
     * Visit a parse tree produced by the {@code quoteless_string} labeled
     * alternative in {@link LodgeParser#value}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitQuoteless_string(LodgeParser.Quoteless_stringContext ctx);

    /**
     * Visit a parse tree produced by {@link LodgeParser#annotation}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAnnotation(LodgeParser.AnnotationContext ctx);

    /**
     * Visit a parse tree produced by {@link LodgeParser#function}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitFunction(LodgeParser.FunctionContext ctx);

    /**
     * Visit a parse tree produced by {@link LodgeParser#function_annotation}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitFunction_annotation(LodgeParser.Function_annotationContext ctx);

    /**
     * Visit a parse tree produced by {@link LodgeParser#error}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitError(LodgeParser.ErrorContext ctx);

    /**
     * Visit a parse tree produced by {@link LodgeParser#conditional}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitConditional(LodgeParser.ConditionalContext ctx);

    /**
     * Visit a parse tree produced by {@link LodgeParser#assignments}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAssignments(LodgeParser.AssignmentsContext ctx);

    /**
     * Visit a parse tree produced by {@link LodgeParser#expressions}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitExpressions(LodgeParser.ExpressionsContext ctx);

    /**
     * Visit a parse tree produced by {@link LodgeParser#separator}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitSeparator(LodgeParser.SeparatorContext ctx);
}
