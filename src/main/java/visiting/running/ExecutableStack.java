package visiting.running;

import common.values.ILodgeValue;
import common.values.standard.LodgeOperator;
import common.values.standard.LodgeQuotelessString;
import static common.values.standard.LodgeUndefined.undefined;
import java.util.Deque;
import java.util.LinkedList;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author Walter Grönholm
 */
class ExecutableStack {

    private final Deque<ILodgeValue> stack;
    private Pair<ILodgeValue, LodgeOperator> currentOperation;

    ExecutableStack() {
        stack = new LinkedList<>();
    }

    ILodgeValue executeWith(IStatementHandler handler) {
        if (stack.isEmpty()) {
            return undefined;
        }
        ILodgeValue key = stack.pollLast();
        ILodgeValue current;
        if (key instanceof LodgeQuotelessString) {
            current = handler.getVariable(key).getValue();
        } else {
            current = key;
        }
        while (!stack.isEmpty()) {
            current = current.given(stack.pollLast());
        }
        if (currentOperation != null) {
            current = currentOperation.getRight().operate(currentOperation.getLeft(), current);
            currentOperation = null;
        }
        return current;
    }

    void add(ILodgeValue value) {
        stack.add(value);
    }

    void setOperation(ILodgeValue left, LodgeOperator operator) {
        currentOperation = Pair.of(left, operator);
    }

}
