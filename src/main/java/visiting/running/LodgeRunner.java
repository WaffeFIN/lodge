package visiting.running;

// Generated from Lodge.g4 by ANTLR 4.7.1
import common.AnnotatedLodgeValue;
import common.values.ILodgeValue;
import common.values.standard.IValueConverter;
import common.values.standard.LodgeQuotelessString;
import common.values.standard.StandardLodgeTypes;
import common.values.standard.StandardLodgeValues;
import common.values.standard.ValueConverter;
import java.io.PrintStream;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;
import visiting.ILodgeVisitor;
import parsing.LodgeParser;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provides an empty implementation of {@link ILodgeVisitor}, which
 * can be extended to create a visitor which only needs to handle a subset of
 * the available methods.
 */
public class LodgeRunner extends AbstractParseTreeVisitor<ILodgeValue> implements ILodgeVisitor<ILodgeValue> {

    private static final Logger LOG = LoggerFactory.getLogger(LodgeRunner.class);
    
    private final IStatementHandler handler;
    private final IValueConverter converter;

    public LodgeRunner(Scanner in, PrintStream out) {
        handler = new StatementHandler(this, defineStaticVariables(new StandardLodgeValues(in, out)));
        converter = new ValueConverter(handler);
    }

    private Deque<Map<ILodgeValue, AnnotatedLodgeValue>> defineStaticVariables(StandardLodgeValues std) {
        Deque<Map<ILodgeValue, AnnotatedLodgeValue>> scopes = new LinkedList<>();

        Map<ILodgeValue, AnnotatedLodgeValue> statics = new HashMap<>();
        statics.put(new LodgeQuotelessString("String"), new AnnotatedLodgeValue(StandardLodgeTypes.Type, StandardLodgeTypes.String, false, AnnotatedLodgeValue.OperatorType.NO_OPERATOR));
        statics.put(new LodgeQuotelessString("Integer"), new AnnotatedLodgeValue(StandardLodgeTypes.Type, StandardLodgeTypes.Integer, false, AnnotatedLodgeValue.OperatorType.NO_OPERATOR));
        statics.put(new LodgeQuotelessString("Boolean"), new AnnotatedLodgeValue(StandardLodgeTypes.Type, StandardLodgeTypes.Boolean, false, AnnotatedLodgeValue.OperatorType.NO_OPERATOR));
        statics.put(new LodgeQuotelessString("print"), new AnnotatedLodgeValue(StandardLodgeTypes.Any, std.print, false, AnnotatedLodgeValue.OperatorType.NO_OPERATOR));
        statics.put(new LodgeQuotelessString("console"), new AnnotatedLodgeValue(StandardLodgeTypes.Any, std.console, false, AnnotatedLodgeValue.OperatorType.NO_OPERATOR));
        statics.put(new LodgeQuotelessString(","), new AnnotatedLodgeValue(StandardLodgeTypes.Any, std.comma, false, AnnotatedLodgeValue.OperatorType.SUFFIX_OPERATOR));
        statics.put(new LodgeQuotelessString("loop"), new AnnotatedLodgeValue(StandardLodgeTypes.Any, std.loop, false, AnnotatedLodgeValue.OperatorType.NO_OPERATOR));
        statics.put(new LodgeQuotelessString("="), new AnnotatedLodgeValue(StandardLodgeTypes.Any, std.equals, false, AnnotatedLodgeValue.OperatorType.BINARY_OPERATOR));
        statics.put(new LodgeQuotelessString("+"), new AnnotatedLodgeValue(StandardLodgeTypes.Any, std.plus, false, AnnotatedLodgeValue.OperatorType.BINARY_OPERATOR));
        statics.put(new LodgeQuotelessString("-"), new AnnotatedLodgeValue(StandardLodgeTypes.Any, std.minus, false, AnnotatedLodgeValue.OperatorType.BINARY_OPERATOR));
        statics.put(new LodgeQuotelessString("div"), new AnnotatedLodgeValue(StandardLodgeTypes.Any, std.division, false, AnnotatedLodgeValue.OperatorType.BINARY_OPERATOR));
        statics.put(new LodgeQuotelessString("program"), new AnnotatedLodgeValue(StandardLodgeTypes.Any, std.program, false, AnnotatedLodgeValue.OperatorType.NO_OPERATOR));
        scopes.add(statics);

        Map<ILodgeValue, AnnotatedLodgeValue> mainScope = new HashMap<>();
        scopes.add(mainScope);

        return scopes;
    }
    
    @Override
    public ILodgeValue visitLodge(LodgeParser.LodgeContext ctx) {
        return visitChildren(ctx);
    }

    @Override
    public ILodgeValue visitStatements(LodgeParser.StatementsContext ctx) {
        return visitChildren(ctx);
    }

    @Override
    public ILodgeValue visitStatement(LodgeParser.StatementContext ctx) {
        return visitChildren(ctx);
    }

    @Override
    public ILodgeValue visitAssignment(LodgeParser.AssignmentContext ctx) {
        return handler.assign(ctx);
    }

    @Override
    public ILodgeValue visitConstant_expression(LodgeParser.Constant_expressionContext ctx) {
        return handler.getKey(ctx);
    }

    @Override
    public ILodgeValue visitExpression(LodgeParser.ExpressionContext ctx) {
        return handler.calculateExpression(ctx);
    }

    @Override
    public ILodgeValue visitFunction(LodgeParser.FunctionContext ctx) {
        return converter.convert(ctx);
    }

    @Override
    public ILodgeValue visitMap(LodgeParser.MapContext ctx) {
        return converter.convert(ctx);
    }

    @Override
    public ILodgeValue visitSet(LodgeParser.SetContext ctx) {
        return converter.convert(ctx);
    }

    @Override
    public ILodgeValue visitEmpty_map(LodgeParser.Empty_mapContext ctx) {
        return converter.convert(ctx);
    }

    @Override
    public ILodgeValue visitTuple(LodgeParser.TupleContext ctx) {
        return converter.convert(ctx);
    }

    @Override
    public ILodgeValue visitEmpty_tuple(LodgeParser.Empty_tupleContext ctx) {
        return converter.convert(ctx);
    }

    @Override
    public ILodgeValue visitNumber(LodgeParser.NumberContext ctx) {
        return converter.convert(ctx);
    }

    @Override
    public ILodgeValue visitBoolean(LodgeParser.BooleanContext ctx) {
        return converter.convert(ctx);
    }

    @Override
    public ILodgeValue visitUndefined(LodgeParser.UndefinedContext ctx) {
        return converter.convert(ctx);
    }

    @Override
    public ILodgeValue visitCharacter(LodgeParser.CharacterContext ctx) {
        return converter.convert(ctx);
    }

    @Override
    public ILodgeValue visitString(LodgeParser.StringContext ctx) {
        return converter.convert(ctx);
    }

    @Override
    public ILodgeValue visitComma(LodgeParser.CommaContext ctx) {
        return converter.convert(ctx);
    }

    @Override
    public ILodgeValue visitQuoteless_string(LodgeParser.Quoteless_stringContext ctx) {
        return converter.convert(ctx);
    }

    @Override
    public ILodgeValue visitConditional(LodgeParser.ConditionalContext ctx) {
        return converter.convert(ctx);
    }

    @Override
    public ILodgeValue visitIdentifier_annotation(LodgeParser.Identifier_annotationContext ctx) {
        return visitChildren(ctx);
    }

    @Override
    public ILodgeValue visitType_annotation(LodgeParser.Type_annotationContext ctx) {
        return visitChildren(ctx);
    }

    @Override
    public ILodgeValue visitFunction_annotation(LodgeParser.Function_annotationContext ctx) {
        return visitChildren(ctx);
    }

    @Override
    public ILodgeValue visitError(LodgeParser.ErrorContext ctx) {
        return converter.convert(ctx);
    }

    @Override
    public ILodgeValue visitAnnotation(LodgeParser.AnnotationContext ctx) {
        return null;
    }

    @Override
    public ILodgeValue visitAssignments(LodgeParser.AssignmentsContext ctx) {
        return visitChildren(ctx);
    }

    @Override
    public ILodgeValue visitExpressions(LodgeParser.ExpressionsContext ctx) {
        return visitChildren(ctx);
    }

    @Override
    public ILodgeValue visitSeparator(LodgeParser.SeparatorContext ctx) {
        return null;
    }

}
