package visiting.running;

import common.AnnotatedLodgeValue;
import common.values.ILodgeValue;
import common.values.standard.LodgeString;
import java.util.Map;
import parsing.LodgeParser;

/**
 *
 * @author Walter Grönholm
 */
public interface IStatementHandler {
    
    public ILodgeValue assign(ILodgeValue key, ILodgeValue value, boolean mutable);

    public ILodgeValue assign(LodgeParser.AssignmentContext assignment);
    
    public ILodgeValue getKey(LodgeParser.Constant_expressionContext ctx);
    
    public ILodgeValue calculateExpression(LodgeParser.ExpressionContext expression);

    public void pushScope();

    public Map<ILodgeValue, AnnotatedLodgeValue> popScope();

    public AnnotatedLodgeValue getVariable(ILodgeValue key);
}
