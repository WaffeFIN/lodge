package visiting.running;

import common.AnnotatedLodgeValue;
import common.values.ILodgeValue;
import common.values.standard.LodgeOperator;
import static common.values.standard.LodgeUndefined.undefined;
import common.values.standard.StandardLodgeTypes;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.antlr.v4.runtime.tree.ParseTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import parsing.LodgeParser;

/**
 *
 * @author Walter Grönholm
 */
public class StatementHandler implements IStatementHandler {

    private static final Logger LOG = LoggerFactory.getLogger(StatementHandler.class);
    private static final AnnotatedLodgeValue annotatedUndefined = new AnnotatedLodgeValue(StandardLodgeTypes.Any, undefined, false, AnnotatedLodgeValue.OperatorType.NO_OPERATOR);

    private final Deque<Map<ILodgeValue, AnnotatedLodgeValue>> scopes;
    private final ExecutableStack executableStack;
    private final LodgeRunner visitor;

    StatementHandler(LodgeRunner visitor, Deque<Map<ILodgeValue, AnnotatedLodgeValue>> scopes) {
        executableStack = new ExecutableStack();
        this.visitor = visitor;
        this.scopes = scopes;
    }

    @Override
    public AnnotatedLodgeValue getVariable(ILodgeValue key) {
        AnnotatedLodgeValue returnValue = annotatedUndefined;
        for (Map<ILodgeValue, AnnotatedLodgeValue> scope : scopes) {
            AnnotatedLodgeValue value = scope.get(key);
            if (value != null) {
                returnValue = value;
            }
        }
        return returnValue;
    }

    private ILodgeValue calculateExpression(List<ILodgeValue> values) {
        for (ILodgeValue value : values) {
            if (value != null) {
                AnnotatedLodgeValue annotatedValue = getVariable(value);
                ILodgeValue left;
                switch (annotatedValue.getOperator()) {
                    case SUFFIX_OPERATOR:
                        left = executableStack.executeWith(this);
                        executableStack.add(annotatedValue.getValue().given(left));
                        break;
                    case PREFIX_OPERATOR:
                    case BINARY_OPERATOR:
                        left = executableStack.executeWith(this);
                        executableStack.setOperation(left, (LodgeOperator) annotatedValue.getValue());
                        break;
                    default:
                        executableStack.add(value);
                }
            }
        }
        return executableStack.executeWith(this);
    }

    @Override
    public ILodgeValue calculateExpression(LodgeParser.ExpressionContext expression) {
        List<ILodgeValue> expressionValues = new ArrayList<>(expression.children.size());
        for (ParseTree child : expression.children) {
            expressionValues.add(child.accept(visitor));
        }
        return calculateExpression(expressionValues);
    }

    @Override
    public ILodgeValue getKey(LodgeParser.Constant_expressionContext ctx) {
        List<LodgeParser.ValueContext> value = ctx.value();
        return value.get(value.size() - 1).accept(visitor);
    }

    @Override
    public ILodgeValue assign(LodgeParser.AssignmentContext assignment) {
        ILodgeValue key = getKey(assignment.constant_expression());
        ILodgeValue value = calculateExpression(assignment.expression());
        boolean mutable = true;
        if ("::".equals(assignment.ASSIGNMENT_OPERATOR().toString())) {
            mutable = false;
        }
        return assign(key, value, mutable);
    }

    @Override
    public ILodgeValue assign(ILodgeValue key, ILodgeValue value, boolean mutable) {
        scopes.peekLast().put(key, new AnnotatedLodgeValue(
            StandardLodgeTypes.Any,
            value,
            mutable,
            AnnotatedLodgeValue.OperatorType.NO_OPERATOR
        ));
        return null;
    }

    @Override
    public void pushScope() {
        scopes.add(new HashMap<>());
    }

    @Override
    public Map<ILodgeValue, AnnotatedLodgeValue> popScope() {
        return scopes.pollLast();
    }

}
