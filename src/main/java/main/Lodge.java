package main;

import java.io.IOException;
import visiting.analyzing.LodgeAnalyzer;
import visiting.running.LodgeRunner;
import common.exceptions.LodgeRuntimeException;
import java.io.PrintStream;
import java.util.Scanner;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import parsing.LodgeLexer;
import parsing.LodgeParser;
import visiting.ILodgeVisitor;

/**
 *
 * @author Walter Grönholm
 */
public class Lodge {

    private static final Logger LOG = LoggerFactory.getLogger(Lodge.class);

    public static void main(String[] args) {
//        if (args.length != 1) {
//            LOG.error("You must provide the source code file name as a program argument");
//        }
//      String fileName = args[0];
        String fileName = "input.lodge";
        LOG.debug("Reading file from " + fileName);
        try {
            run(CharStreams.fromFileName(fileName), new Scanner(System.in), System.out);
        } catch (IOException ex) {
            LOG.error("Error reading file " + fileName, ex);
        } catch (RecognitionException ex) {
            LOG.error("Parsing error in " + fileName, ex);
        } catch (LodgeRuntimeException ex) {
            LOG.error("Runtime error in " + fileName, ex);
        } catch (AbstractLodgeException ex) {
            LOG.error("Error in " + fileName, ex);
        } catch (Exception ex) {
            LOG.error("Unexpected exception occured!", ex);
        }
    }

    static void run(CharStream inputStream, Scanner in, PrintStream out) throws Exception {
        LOG.debug("Parsing...");
        LodgeLexer lodgeLexer = new LodgeLexer(inputStream);
        CommonTokenStream commonTokenStream = new CommonTokenStream(lodgeLexer);
        LodgeParser lodgeParser = new LodgeParser(commonTokenStream);
        LodgeParser.LodgeContext lodgeContext = lodgeParser.lodge();
        LOG.debug("Analyzing...");
        ILodgeVisitor analyzer = new LodgeAnalyzer();
        analyzer.visitLodge(lodgeContext);
        LOG.debug("Running...");
        ILodgeVisitor runner = new LodgeRunner(in, out);
        runner.visitLodge(lodgeContext);
    }
}
