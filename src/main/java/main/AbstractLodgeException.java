package main;

/**
 *
 * @author Walter Grönholm
 */
public abstract class AbstractLodgeException extends RuntimeException {

    public AbstractLodgeException(String message) {
        super(message);
    }
    
}
